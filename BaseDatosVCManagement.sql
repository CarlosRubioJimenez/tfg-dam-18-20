CREATE DATABASE baseVCManagement;
--
USE baseVCManagement;
--
CREATE TABLE IF NOT EXISTS tiendas (
id int primary key auto_increment,
nombre varchar(50),
direccion varchar(60),
fecha_apertura date,
puntuacion float,
telefono varchar(9),
email varchar(50),
num_empleados int,
num_ventas int
);
--
CREATE TABLE IF NOT EXISTS empleados (
id int primary key auto_increment,
nombre varchar(200),
apellidos varchar(200),
dni varchar(100),
telefono varchar(100),
sexo varchar(50),
edad int,
fecha_contratacion date,
email varchar(200),
idTienda int,
FOREIGN KEY (idTienda) references tiendas (id)
);
--
CREATE TABLE IF NOT EXISTS videojuegos (
id int primary key auto_increment,
nombre varchar(200),
genero varchar(200),
precio float,
fecha_salida date,
puntuacion float,
plataforma varchar(200),
edad_minima int
);
--
CREATE TABLE IF NOT EXISTS clientes (
id int primary key auto_increment,
nombre varchar(200),
apellidos varchar(200),
usuario varchar(200),
contrasena varchar(200),
num_compras int,
fecha_inscripcion date,
email varchar(200)
);

CREATE TABLE IF NOT EXISTS tienda_cliente (
idTienda int,
idCliente int,
FOREIGN KEY (idTienda) REFERENCES tiendas (id),
FOREIGN KEY (idCliente) REFERENCES clientes (id),
PRIMARY KEY (idTienda, idCliente)
);

CREATE TABLE IF NOT EXISTS tienda_videojuego (
idTienda int,
idVideojuego int,
FOREIGN KEY (idTienda) REFERENCES tiendas (id),
FOREIGN KEY (idVideojuego) REFERENCES videojuegos (id),
PRIMARY KEY (idTienda, idVideojuego)
);

CREATE TABLE IF NOT EXISTS usuarios (
id int auto_increment primary key,
usuario varchar(200),
contrasena varchar(200),
rol varchar(200)
);