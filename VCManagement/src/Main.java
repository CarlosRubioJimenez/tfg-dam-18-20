import com.vcmanagement.gui.Controlador;
import com.vcmanagement.gui.Modelo;
import com.vcmanagement.gui.Vista;

public class Main {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}