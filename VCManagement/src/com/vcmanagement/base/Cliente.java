package com.vcmanagement.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "clientes", schema = "basevcmanagement", catalog = "")
public class Cliente{
    private int id;
    private String nombre;
    private String apellidos;
    private String usuario;
    private String contrasena;
    private int numCompras;
    private Date fechaInscripcion;
    private String email;
    private List<Tienda> tiendas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "usuario")
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Basic
    @Column(name = "contrasena")
    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    @Basic
    @Column(name = "num_compras")
    public int getNumCompras() {
        return numCompras;
    }

    public void setNumCompras(int numCompras) {
        this.numCompras = numCompras;
    }

    @Basic
    @Column(name = "fecha_inscripcion")
    public Date getFechaInscripcion() {
        return fechaInscripcion;
    }

    public void setFechaInscripcion(Date fechaInscripcion) {
        this.fechaInscripcion = fechaInscripcion;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return id == cliente.id &&
                numCompras == cliente.numCompras &&
                Objects.equals(nombre, cliente.nombre) &&
                Objects.equals(apellidos, cliente.apellidos) &&
                Objects.equals(usuario, cliente.usuario) &&
                Objects.equals(contrasena, cliente.contrasena) &&
                Objects.equals(fechaInscripcion, cliente.fechaInscripcion) &&
                Objects.equals(email, cliente.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, usuario, contrasena, numCompras, fechaInscripcion, email);
    }

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name = "tienda_cliente", catalog = "", schema = "basevcmanagement", joinColumns = @JoinColumn(name = "idCliente", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "idTienda", referencedColumnName = "id", nullable = false))
    public List<Tienda> getTiendas() {
        return tiendas;
    }

    public void setTiendas(List<Tienda> tiendas) {
        this.tiendas = tiendas;
    }

    public String toString() {
        return this.nombre + " " + this.apellidos + " | Usuario: " + this.usuario + " | Email: " + this.email;
    }
}
