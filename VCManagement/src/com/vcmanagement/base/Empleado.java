package com.vcmanagement.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "empleados", schema = "basevcmanagement", catalog = "")
public class Empleado {
    private int id;
    private String nombre;
    private String apellidos;
    private String dni;
    private String telefono;
    private String sexo;
    private int edad;
    private Date fechaContratacion;
    private String email;
    private Tienda tienda;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "sexo")
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @Basic
    @Column(name = "edad")
    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Basic
    @Column(name = "fecha_contratacion")
    public Date getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(Date fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empleado empleado = (Empleado) o;
        return id == empleado.id &&
                edad == empleado.edad &&
                Objects.equals(nombre, empleado.nombre) &&
                Objects.equals(apellidos, empleado.apellidos) &&
                Objects.equals(dni, empleado.dni) &&
                Objects.equals(telefono, empleado.telefono) &&
                Objects.equals(sexo, empleado.sexo) &&
                Objects.equals(fechaContratacion, empleado.fechaContratacion) &&
                Objects.equals(email, empleado.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, dni, telefono, sexo, edad, fechaContratacion, email);
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "idTienda", referencedColumnName = "id")
    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }

    public String toString() {
        return this.nombre + " " + this.apellidos + " | DNI: " + this.dni + " | Teléfono: " + this.telefono
                + " | Email: " + this.email + " | Sexo: " + this.sexo;
    }
}
