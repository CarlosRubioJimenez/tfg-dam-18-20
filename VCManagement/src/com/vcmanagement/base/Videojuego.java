package com.vcmanagement.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "videojuegos", schema = "basevcmanagement", catalog = "")
public class Videojuego {
    private int id;
    private String nombre;
    private String genero;
    private float precio;
    private Date fechaSalida;
    private float puntuacion;
    private String plataforma;
    private int edadMinima;
    private List<Tienda> tiendas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "genero")
    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Basic
    @Column(name = "precio")
    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "fecha_salida")
    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    @Basic
    @Column(name = "puntuacion")
    public float getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(float puntuacion) {
        this.puntuacion = puntuacion;
    }

    @Basic
    @Column(name = "plataforma")
    public String getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    @Basic
    @Column(name = "edad_minima")
    public int getEdadMinima() {
        return edadMinima;
    }

    public void setEdadMinima(int edadMinima) {
        this.edadMinima = edadMinima;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Videojuego that = (Videojuego) o;
        return id == that.id &&
                Float.compare(that.precio, precio) == 0 &&
                Float.compare(that.puntuacion, puntuacion) == 0 &&
                edadMinima == that.edadMinima &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(genero, that.genero) &&
                Objects.equals(fechaSalida, that.fechaSalida) &&
                Objects.equals(plataforma, that.plataforma);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, genero, precio, fechaSalida, puntuacion, plataforma, edadMinima);
    }

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name = "tienda_videojuego", catalog = "", schema = "basevcmanagement", joinColumns = @JoinColumn(name = "idVideojuego", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "idTienda", referencedColumnName = "id", nullable = false))
    public List<Tienda> getTiendas() {
        return tiendas;
    }

    public void setTiendas(List<Tienda> tiendas) {
        this.tiendas = tiendas;
    }

    public String toString() {
        return this.nombre + " | Precio: " + this.precio + " | Plataforma/s: " + this.plataforma + " | Edad mínima: " + this.edadMinima;
    }
}
