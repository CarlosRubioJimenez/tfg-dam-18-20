package com.vcmanagement.base;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "usuarios", schema = "basevcmanagement", catalog = "")
public class Usuario {
    private int id;
    private String usuario;
    private String contrasena;
    private String rol;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "usuario")
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Basic
    @Column(name = "contrasena")
    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    @Basic
    @Column(name = "rol")
    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario1 = (Usuario) o;
        return id == usuario1.id &&
                Objects.equals(usuario, usuario1.usuario) &&
                Objects.equals(contrasena, usuario1.contrasena) &&
                Objects.equals(rol, usuario1.rol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, usuario, contrasena, rol);
    }

    public String toString() {
        return this.usuario + " | Rol: " + this.rol;
    }
}
