package com.vcmanagement.base;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "tiendas", schema = "basevcmanagement", catalog = "")
public class Tienda {
    private int id;
    private String nombre;
    private String direccion;
    private Date fechaApertura;
    private float puntuacion;
    private String telefono;
    private String email;
    private int numEmpleados;
    private int numVentas;
    private List<Empleado> empleados;
    private List<Cliente> clientes;
    private List<Videojuego> videojuegos;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "fecha_apertura")
    public Date getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(Date fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    @Basic
    @Column(name = "puntuacion")
    public float getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(float puntuacion) {
        this.puntuacion = puntuacion;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "num_empleados")
    public int getNumEmpleados() {
        return numEmpleados;
    }

    public void setNumEmpleados(int numEmpleados) {
        this.numEmpleados = numEmpleados;
    }

    @Basic
    @Column(name = "num_ventas")
    public int getNumVentas() {
        return numVentas;
    }

    public void setNumVentas(int numVentas) {
        this.numVentas = numVentas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tienda tienda = (Tienda) o;
        return id == tienda.id &&
                Float.compare(tienda.puntuacion, puntuacion) == 0 &&
                numEmpleados == tienda.numEmpleados &&
                numVentas == tienda.numVentas &&
                Objects.equals(nombre, tienda.nombre) &&
                Objects.equals(direccion, tienda.direccion) &&
                Objects.equals(fechaApertura, tienda.fechaApertura) &&
                Objects.equals(telefono, tienda.telefono) &&
                Objects.equals(email, tienda.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, direccion, fechaApertura, puntuacion, telefono, email, numEmpleados, numVentas);
    }

    @OneToMany(mappedBy = "tienda", cascade = CascadeType.DETACH)
    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }

    @ManyToMany(mappedBy = "tiendas", cascade = CascadeType.DETACH)
    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    @ManyToMany(mappedBy = "tiendas", cascade = CascadeType.DETACH)
    public List<Videojuego> getVideojuegos() {
        return videojuegos;
    }

    public void setVideojuegos(List<Videojuego> videojuegos) {
        this.videojuegos = videojuegos;
    }

    public String toString() {
        return this.nombre + " | Dirección: " + this.direccion + " | Teléfono: " + this.telefono
                + " | Email: " + this.email + " | Puntuación: " + this.puntuacion;
    }
}
