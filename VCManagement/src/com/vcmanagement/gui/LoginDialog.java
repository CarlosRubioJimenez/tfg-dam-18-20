package com.vcmanagement.gui;

import com.vcmanagement.base.Usuario;
import com.vcmanagement.util.Util;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Clase LoginDialog que extiende JDialog y se encarga de crear un JDialog
 * para gestionar el login de la aplicación
 */
public class LoginDialog extends JDialog {
    private JButton btnLogin;
    private JTextField txtUsuarioLogin;
    private JPasswordField txtContrasenaLogin;
    private JPanel contentPane;

    private Modelo modelo;
    private String rol;
    private List<Usuario> usuarios;

    /**
     * Constructor de la clase LoginDialog que recibe el modelo y obtiene los usuarios de la base de datos.
     * Crea la interfaz gráfica y añade los listeners
     * @param modelo
     */
    public LoginDialog(Modelo modelo) {

        this.modelo = modelo;
        this.rol = "";
        this.usuarios = modelo.getUsuarios();

        setContentPane(contentPane);
        setModal(true);
        setLocationRelativeTo(null);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();

        anadirListeners();

        setTitle("Login ");
    }

    /**
     * Método encargado de añadir el listener al botón del JDialog y gestionar su acción
     */
    private void anadirListeners() {

        btnLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String usuario = txtUsuarioLogin.getText();
                String contrasena = txtContrasenaLogin.getText();
                Usuario usuarioBuscado = null;

                for(Usuario usuarioItem : usuarios) {
                    if(usuarioItem.getUsuario().equals(usuario)) {
                        usuarioBuscado = usuarioItem;
                    }
                }

                if (usuarioBuscado != null) {
                    if (usuarioBuscado.getContrasena().equals(contrasena)) {
                        rol = usuarioBuscado.getRol();
                        dispose();
                    }
                    else {
                        Util.mensajeError("Contraseña incorrecta");
                    }
                }

                if (usuarioBuscado == null) {
                    Util.mensajeError("Usuario no encontrado");
                }
            }
        });
    }

    /**
     * Método encargado de obtener el rol del usuario registrado
     * @return
     */
    public String getRol() {
        return this.rol;
    }

    /**
     * Método encargado de hacer visible el JDialog
     */
    public void mostrarDialogo() {
        setVisible(true);
    }
}
