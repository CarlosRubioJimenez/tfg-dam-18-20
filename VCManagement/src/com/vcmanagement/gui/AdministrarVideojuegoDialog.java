package com.vcmanagement.gui;

import com.vcmanagement.base.Tienda;
import com.vcmanagement.base.Videojuego;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Clase AdministrarVideojuegoDialog que extiende JDialog y se encarga de crear un JDialog
 * para gestionar los videojuegos de una tienda
 */
public class AdministrarVideojuegoDialog extends JDialog{
    private JButton btnEliminarVideojuegoTienda;
    private JList listVideojuegosTienda;
    private JButton btnAnadirVideojuegoTienda;
    private JList listVideojuegosExistentes;
    private JButton btnOk;
    private JPanel contentPane;

    private Modelo modelo;
    private Tienda tienda;

    private DefaultListModel<Videojuego> dlmVideojuegosTienda;
    private DefaultListModel<Videojuego> dlmVideojuegosExistentes;

    /**
     * Constructor de la clase AdministrarVideojuegoDialog que recibe una tienda y el modelo. Crea la interfaz gráfica,
     * asigna los Listeners y los DefaultListModels a las listas
     * @param tienda Objeto Tienda a administrar
     * @param modelo
     */
    public AdministrarVideojuegoDialog(Tienda tienda, Modelo modelo) {
        this.tienda = tienda;
        this.modelo = modelo;

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btnOk);
        setLocationRelativeTo(null);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();

        anadirListeners();

        iniciarListas();
        setTitle("Tienda: " + tienda.getNombre());
    }

    /**
     * Método encargado de añadir los listeners y gestionar las acciones de cada botón
     */
    public void anadirListeners() {
        btnOk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        btnEliminarVideojuegoTienda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Videojuego videojuegoSeleccionado = (Videojuego) listVideojuegosTienda.getSelectedValue();

                if (videojuegoSeleccionado == null) {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un videojuego",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
                else {
                    tienda.getVideojuegos().remove(videojuegoSeleccionado);
                    videojuegoSeleccionado.getTiendas().remove(tienda);

                    modelo.guardarVideojuego(videojuegoSeleccionado);
                    modelo.guardarTienda(tienda);

                    listarDatos();
                }
            }
        });

        btnAnadirVideojuegoTienda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Videojuego videojuegoSeleccionado = (Videojuego) listVideojuegosExistentes.getSelectedValue();

                if (videojuegoSeleccionado == null) {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un videojuego",
                            "Error", JOptionPane.ERROR_MESSAGE);                }
                else {
                    tienda.getVideojuegos().add(videojuegoSeleccionado);
                    videojuegoSeleccionado.getTiendas().add(tienda);

                    modelo.guardarVideojuego(videojuegoSeleccionado);
                    modelo.guardarTienda(tienda);

                    listarDatos();
                }
            }
        });
    }

    /**
     * Método que cierra el JDialog
     */
    private void onOK() {
        dispose();
    }

    /**
     * Método que lista los empleados de una tienda y de la aplicación en los Jlists del JDialog y lo hace visible
     */
    public void mostrarDialogo() {
        listarDatos();
        setVisible(true);
    }

    /**
     * Método que inicializa los DefaultListModels y se encarga de asignarlos a sus respectivos Jlists
     */
    public void iniciarListas() {
        dlmVideojuegosTienda = new DefaultListModel<>();
        listVideojuegosTienda.setModel(dlmVideojuegosTienda);

        dlmVideojuegosExistentes = new DefaultListModel<>();
        listVideojuegosExistentes.setModel(dlmVideojuegosExistentes);

    }

    /**
     * Método encargado de llamar a todos los métodos para listar
     */
    public void listarDatos() {
        listarVideojuegosExistentes(modelo.getVideojuegos());
        listarVideojuegosTienda();
    }

    /**
     * Método encargado de listar los videojuegos de una tienda
     */
    public void listarVideojuegosTienda() {
        dlmVideojuegosTienda.clear();
        for (Videojuego videojuego : tienda.getVideojuegos()) {
            dlmVideojuegosTienda.addElement(videojuego);
        }
    }

    /**
     * Método encargado de listar los videojuegos de la aplicación que no estén asignados a ninguna tienda
     * @param videojuegos
     */
    private void listarVideojuegosExistentes(List<Videojuego> videojuegos) {
        dlmVideojuegosExistentes.clear();

        List<Videojuego> videojuegosTienda = tienda.getVideojuegos();
        boolean encontrado = false;

        for(Videojuego videojuego : videojuegos){
            encontrado = false;
            for (Videojuego videojuegoTienda : videojuegosTienda) {
                if(videojuego.getId() == videojuegoTienda.getId()) {
                    encontrado = true;
                }
            }
            if (!encontrado) {
                dlmVideojuegosExistentes.addElement(videojuego);
            }
        }
    }
}
