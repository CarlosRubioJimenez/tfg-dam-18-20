package com.vcmanagement.gui;

import com.vcmanagement.base.*;
import com.vcmanagement.util.Util;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

/**
 * Clase Controlador que implementa ActionListner, ListSelectioListener y KeyListener.
 * Se encarga de hacer de nexo entre el Modelo y la Vista, gestionando las acciones entre ellos.
 */
public class Controlador implements ActionListener, ListSelectionListener, KeyListener {
    Vista vista;
    Modelo modelo;
    String rol;

    /**
     * Constructor de la clase Controlador que recibe una Vista y un Modelo, añade los listeners,
     * Abre el JDialog de login y se encarga de gestionar los botones.
     *
     * @param vista Objeto vista encargado de generar la interfaz gráfica
     * @param modelo Objeto modelo encargado de gestionar la información de la aplicación
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        anadirActionListeners(this);
        anadirListSelectionListeners(this);
        anadirKeyListeners(this);

        desactivarVista();

        modelo.conectar();

        LoginDialog dialogLogin = new LoginDialog(modelo);
        dialogLogin.mostrarDialogo();

        modelo.desconectar();

        this.rol = dialogLogin.getRol();

        vista.itemConectar.setEnabled(true);
    }

    /**
     * Método encargado de desavtivar todos los componentes de la Vista
     */
    private void desactivarVista() {
        // Menú
        vista.itemConectar.setEnabled(false);
        vista.itemAdministrarUsuarios.setEnabled(false);
        vista.itemCerrarSesion.setEnabled(false);

        // Tiendas
        vista.btnAltaTienda.setEnabled(false);
        vista.btnModificarTienda.setEnabled(false);
        vista.btnEliminarTienda.setEnabled(false);
        vista.btnAdministrarEmpleadosTienda.setEnabled(false);
        vista.btnAdministrarClientesTienda.setEnabled(false);
        vista.btnAdministrarVideojuegosTienda.setEnabled(false);
        vista.btnLimpiarCamposTienda.setEnabled(false);
        vista.btnInformeTiendas.setEnabled(false);
        vista.cbBuscarTienda.setEnabled(false);
        vista.txtBuscarTienda.setEnabled(false);
        vista.datePickerFechaAperturaTienda.setEnabled(false);
        vista.spNumeroVentasTienda.setEnabled(false);
        vista.txtNombreTienda.setEnabled(false);
        vista.txtDireccionTienda.setEnabled(false);
        vista.txtPuntuacionTienda.setEnabled(false);
        vista.txtTelefonoTienda.setEnabled(false);
        vista.txtEmailTienda.setEnabled(false);

        // Empleados
        vista.btnAltaEmpleado.setEnabled(false);
        vista.btnEliminarEmpleado.setEnabled(false);
        vista.btnModificarEmpleado.setEnabled(false);
        vista.btnLimpiarCamposEmpleado.setEnabled(false);
        vista.btnInformeEmpleados.setEnabled(false);
        vista.cbSexoEmpleado.setEnabled(false);
        vista.cbBuscarEmpleado.setEnabled(false);
        vista.txtBuscarEmpleado.setEnabled(false);
        vista.spEdadEmpleado.setEnabled(false);
        vista.datePickerContratacionEmpleado.setEnabled(false);
        vista.txtNombreEmpleado.setEnabled(false);
        vista.txtApellidosEmpleado.setEnabled(false);
        vista.txtDniEmpleado.setEnabled(false);
        vista.txtTelefonoEmpleado.setEnabled(false);
        vista.txtEmailEmpleado.setEnabled(false);

        // Cliente
        vista.btnAltaCliente.setEnabled(false);
        vista.btnEliminarCliente.setEnabled(false);
        vista.btnModificarCliente.setEnabled(false);
        vista.btnLimpiarCamposCliente.setEnabled(false);
        vista.btnInformeClientes.setEnabled(false);
        vista.cbBuscarCliente.setEnabled(false);
        vista.txtBuscarCliente.setEnabled(false);
        vista.spNumeroComprasCliente.setEnabled(false);
        vista.datePickerInscripcionCliente.setEnabled(false);
        vista.txtNombreCliente.setEnabled(false);
        vista.txtApellidosCliente.setEnabled(false);
        vista.txtUsarioCliente.setEnabled(false);
        vista.txtContrasenaCliente.setEnabled(false);
        vista.txtEmailCliente.setEnabled(false);

        // Videojuego
        vista.btnAltaVideojuego.setEnabled(false);
        vista.btnEliminarVideojuego.setEnabled(false);
        vista.btnModificarVideojuego.setEnabled(false);
        vista.btnLimpiarCamposVideojuego.setEnabled(false);
        vista.btnInformeVideojuegos.setEnabled(false);
        vista.cbBuscarVideojuego.setEnabled(false);
        vista.txtBuscarVideojuego.setEnabled(false);
        vista.spEdadMinimaJugarVideojuego.setEnabled(false);
        vista.spEdadMinimaJugarVideojuego.setEnabled(false);
        vista.txtNombreVideojuego.setEnabled(false);
        vista.txtGeneroVideojuego.setEnabled(false);
        vista.txtPrecioVideojuego.setEnabled(false);
        vista.txtPuntuacionVideojuego.setEnabled(false);
        vista.txtPlataformaVideojuego.setEnabled(false);
        vista.datePickerSalidaVideojuego.setEnabled(false);
    }

    /**
     * Método encargado de administrar los componentes de la vista dependiendo del rol del usuario
     */
    private void administrarComponentesVista() {
        // Componentes que siempre se activan
        vista.itemConectar.setEnabled(true);
        vista.itemCerrarSesion.setEnabled(true);

        vista.btnInformeTiendas.setEnabled(true);
        vista.btnInformeEmpleados.setEnabled(true);
        vista.btnInformeClientes.setEnabled(true);
        vista.btnInformeVideojuegos.setEnabled(true);

        if (rol.equalsIgnoreCase("administrador") || rol.equalsIgnoreCase("editor")) {

            // Tiendas
            vista.btnAltaTienda.setEnabled(true);
            vista.btnModificarTienda.setEnabled(true);
            vista.btnEliminarTienda.setEnabled(true);
            vista.btnAdministrarEmpleadosTienda.setEnabled(true);
            vista.btnAdministrarClientesTienda.setEnabled(true);
            vista.btnAdministrarVideojuegosTienda.setEnabled(true);
            vista.btnLimpiarCamposTienda.setEnabled(true);
            vista.cbBuscarTienda.setEnabled(true);
            vista.txtBuscarTienda.setEnabled(true);
            vista.datePickerFechaAperturaTienda.setEnabled(true);
            vista.spNumeroVentasTienda.setEnabled(true);
            vista.txtNombreTienda.setEnabled(true);
            vista.txtDireccionTienda.setEnabled(true);
            vista.txtPuntuacionTienda.setEnabled(true);
            vista.txtTelefonoTienda.setEnabled(true);
            vista.txtEmailTienda.setEnabled(true);

            // Empleados
            vista.btnLimpiarCamposEmpleado.setEnabled(true);
            vista.cbSexoEmpleado.setEnabled(true);
            vista.cbBuscarEmpleado.setEnabled(true);
            vista.txtBuscarEmpleado.setEnabled(true);
            vista.spEdadEmpleado.setEnabled(true);
            vista.datePickerContratacionEmpleado.setEnabled(true);
            vista.btnAltaEmpleado.setEnabled(true);
            vista.btnEliminarEmpleado.setEnabled(true);
            vista.btnModificarEmpleado.setEnabled(true);
            vista.txtNombreEmpleado.setEnabled(true);
            vista.txtApellidosEmpleado.setEnabled(true);
            vista.txtDniEmpleado.setEnabled(true);
            vista.txtTelefonoEmpleado.setEnabled(true);
            vista.txtEmailEmpleado.setEnabled(true);

            // Clientes
            vista.btnLimpiarCamposCliente.setEnabled(true);
            vista.cbBuscarCliente.setEnabled(true);
            vista.txtBuscarCliente.setEnabled(true);
            vista.spNumeroComprasCliente.setEnabled(true);
            vista.datePickerInscripcionCliente.setEnabled(true);
            vista.btnAltaCliente.setEnabled(true);
            vista.btnEliminarCliente.setEnabled(true);
            vista.btnModificarCliente.setEnabled(true);
            vista.txtNombreCliente.setEnabled(true);
            vista.txtApellidosCliente.setEnabled(true);
            vista.txtUsarioCliente.setEnabled(true);
            vista.txtContrasenaCliente.setEnabled(true);
            vista.txtEmailCliente.setEnabled(true);


            // Videojuegos
            vista.btnLimpiarCamposVideojuego.setEnabled(true);
            vista.cbBuscarVideojuego.setEnabled(true);
            vista.txtBuscarVideojuego.setEnabled(true);
            vista.spEdadMinimaJugarVideojuego.setEnabled(true);
            vista.spEdadMinimaJugarVideojuego.setEnabled(true);
            vista.btnAltaVideojuego.setEnabled(true);
            vista.btnEliminarVideojuego.setEnabled(true);
            vista.btnModificarVideojuego.setEnabled(true);
            vista.txtNombreVideojuego.setEnabled(true);
            vista.txtGeneroVideojuego.setEnabled(true);
            vista.txtPrecioVideojuego.setEnabled(true);
            vista.txtPuntuacionVideojuego.setEnabled(true);
            vista.txtPlataformaVideojuego.setEnabled(true);
            vista.datePickerSalidaVideojuego.setEnabled(true);
        }

        if (rol.equalsIgnoreCase("administrador")) {
            // Menú
            vista.itemAdministrarUsuarios.setEnabled(true);
        }

        if (rol.equalsIgnoreCase("consultor")) {
            vista.cbBuscarTienda.setEnabled(true);
            vista.txtBuscarTienda.setEnabled(true);

            vista.cbBuscarEmpleado.setEnabled(true);
            vista.txtBuscarEmpleado.setEnabled(true);

            vista.cbBuscarCliente.setEnabled(true);
            vista.txtBuscarCliente.setEnabled(true);

            vista.cbBuscarVideojuego.setEnabled(true);
            vista.txtBuscarVideojuego.setEnabled(true);
        }
    }

    /**
     * Método encargado de añadir los ActionListeners a los componentes de la vista
     * @param listener
     */
    private void anadirActionListeners(ActionListener listener) {

        // Componentes de Tienda
        vista.btnAltaTienda.addActionListener(listener);
        vista.btnEliminarTienda.addActionListener(listener);
        vista.btnModificarTienda.addActionListener(listener);
        vista.btnLimpiarCamposTienda.addActionListener(listener);
        vista.btnAdministrarEmpleadosTienda.addActionListener(listener);
        vista.btnAdministrarClientesTienda.addActionListener(listener);
        vista.btnAdministrarVideojuegosTienda.addActionListener(listener);
        vista.btnInformeTiendas.addActionListener(listener);

        // Componentes de Empleado
        vista.btnAltaEmpleado.addActionListener(listener);
        vista.btnModificarEmpleado.addActionListener(listener);
        vista.btnEliminarEmpleado.addActionListener(listener);
        vista.btnLimpiarCamposEmpleado.addActionListener(listener);
        vista.btnInformeEmpleados.addActionListener(listener);

        // Componentes de Cliente
        vista.btnAltaCliente.addActionListener(listener);
        vista.btnModificarCliente.addActionListener(listener);
        vista.btnEliminarCliente.addActionListener(listener);
        vista.btnLimpiarCamposCliente.addActionListener(listener);
        vista.btnInformeClientes.addActionListener(listener);

        // Componentes de Videojuego
        vista.btnAltaVideojuego.addActionListener(listener);
        vista.btnModificarVideojuego.addActionListener(listener);
        vista.btnEliminarVideojuego.addActionListener(listener);
        vista.btnLimpiarCamposVideojuego.addActionListener(listener);
        vista.btnInformeVideojuegos.addActionListener(listener);

        // Componentes del menú
        vista.itemConectar.addActionListener(listener);
        vista.itemAdministrarUsuarios.addActionListener(listener);
        vista.itemCerrarSesion.addActionListener(listener);

    }

    /**
     * Método encargado de añadir los ListSelectionListeners a los componentes de la Vista
     * @param listener
     */
    private void anadirListSelectionListeners(ListSelectionListener listener) {

        // Componentes de Tienda
        vista.listTiendas.addListSelectionListener(listener);
        vista.listEmpleadosTienda.addListSelectionListener(listener);
        vista.listClientesTienda.addListSelectionListener(listener);
        vista.listVideojuegosTienda.addListSelectionListener(listener);

        // Componente de Empleado
        vista.listEmpleados.addListSelectionListener(listener);

        // Componente de Cliente
        vista.listClientes.addListSelectionListener(listener);
        vista.listTiendasCliente.addListSelectionListener(listener);

        // Componente de Videojuego
        vista.listVideojuegos.addListSelectionListener(listener);
        vista.listTiendaVideojuego.addListSelectionListener(listener);
    }

    /**
     * Método encargado de añadir los KeyListeners a los componentes de la Vista
     * @param listener
     */
    private void anadirKeyListeners(KeyListener listener) {
        vista.txtBuscarTienda.addKeyListener(listener);
        vista.txtBuscarEmpleado.addKeyListener(listener);
        vista.txtBuscarCliente.addKeyListener(listener);
        vista.txtBuscarVideojuego.addKeyListener(listener);
    }

    // **** MÉTODOS ENCARGADOS DE LISTAR ****

    /**
     * Método encargado de llamar todos los métodos de listar
     */
    private void listarTodo() {
        listarTiendas(modelo.getTiendas());
        listarClientes(modelo.getClientes());
        listarEmpleados(modelo.getEmpleados());
        listarVideojuegos(modelo.getVideojuegos());
    }

    /**
     * Método encargado de listar las tiendas de la aplicación
     * @param tiendas List de objetos Tienda de la aplicación
     */
    private void listarTiendas(List<Tienda> tiendas) {
        vista.dlmTiendas.clear();
        for(Tienda tienda : tiendas){
            vista.dlmTiendas.addElement(tienda);
        }
    }

    /**
     * Método encargado de listar los empleados de una tienda
     * @param empleados List de objetos Empleado de una tienda
     */
    private void listarEmpleadosTienda(List<Empleado> empleados) {
        vista.dlmEmpleadosTienda.clear();
        for(Empleado empleado : empleados){
            vista.dlmEmpleadosTienda.addElement(empleado);
        }
    }

    /**
     * Método encargado de listar los clientes de una tienda
     * @param clientes List de objetos Cliente de una tienda
     */
    private void listarClientesTienda(List<Cliente> clientes) {
        vista.dlmClientesTienda.clear();
        for(Cliente cliente : clientes){
            vista.dlmClientesTienda.addElement(cliente);
        }
    }

    /**
     * Método encargado de listar los videojuegos de una tienda
     * @param videojuegos List de objetos Videojuego de una tienda
     */
    private void listarVideojuegosTienda(List<Videojuego> videojuegos) {
        vista.dlmVideojuegosTienda.clear();
        for(Videojuego videojuego : videojuegos){
            vista.dlmVideojuegosTienda.addElement(videojuego);
        }
    }

    /**
     * Método encargado de listar los empleados de la aplicación
     * @param empleados List de objetos Empleado de la aplicación
     */
    private void listarEmpleados(List<Empleado> empleados) {
        vista.dlmEmpleados.clear();
        for(Empleado empleado : empleados){
            vista.dlmEmpleados.addElement(empleado);
        }
    }

    /**
     * Método encargado de listar los clientes de la aplicación
     * @param clientes List de objetos Cliente de la aplicación
     */
    private void listarClientes(List<Cliente> clientes) {
        vista.dlmClientes.clear();
        for(Cliente cliente : clientes){
            vista.dlmClientes.addElement(cliente);
        }
    }

    /**
     * Método encargado de listar las tiendas de un cliente
     * @param tiendas List de objetos Tienda de un cliente
     */
    private void listarTiendasCliente(List<Tienda> tiendas) {
        vista.dlmTiendasCliente.clear();
        for(Tienda tienda : tiendas){
            vista.dlmTiendasCliente.addElement(tienda);
        }
    }

    /**
     * Método encargado de listar los videojuegos de la aplicación
     * @param videojuegos List de objetos Videojuego de la aplicación
     */
    private void listarVideojuegos(List<Videojuego> videojuegos) {
        vista.dlmVideojuegos.clear();
        for(Videojuego videojuego : videojuegos){
            vista.dlmVideojuegos.addElement(videojuego);
        }
    }

    /**
     * Método encargado de listar las tiendas de un videojuego
     * @param tiendas List de objetos Tienda de un videojuego
     */
    private void listarTiendasVideojuego(List<Tienda> tiendas) {
        vista.dlmTiendasVideojuego.clear();
        for(Tienda tienda : tiendas){
            vista.dlmTiendasVideojuego.addElement(tienda);
        }
    }

    // **** MÉTODOS ENCARGADOS DE LIMPIAR CAMPOS ****

    /**
     * Método encargado de vaciar los campos del formulario de tienda
     */
    private void limpiarCamposTienda() {
        vista.txtNombreTienda.setText("");
        vista.txtDireccionTienda.setText("");
        vista.datePickerFechaAperturaTienda.setDate(null);
        vista.txtPuntuacionTienda.setText("");
        vista.txtTelefonoTienda.setText("");
        vista.txtEmailTienda.setText("");
        vista.spNumeroVentasTienda.setValue(0);

        vista.dlmEmpleadosTienda.clear();
        vista.dlmClientesTienda.clear();
        vista.dlmVideojuegosTienda.clear();
    }

    /**
     * Método encargado de vaciar los campos del formulario de empleado
     */
    private void limpiarCamposEmpleado() {
        vista.txtNombreEmpleado.setText("");
        vista.txtApellidosEmpleado.setText("");
        vista.txtDniEmpleado.setText("");
        vista.txtTelefonoEmpleado.setText("");
        vista.spEdadEmpleado.setValue(0);
        vista.txtEmailEmpleado.setText("");
        vista.datePickerContratacionEmpleado.setDate(null);
        vista.txtTiendaEmpleado.setText("");
    }

    /**
     * Método encargado de vaciar los campos del formulario de cliente
     */
    private void limpiarCamposCliente() {
        vista.txtNombreCliente.setText("");
        vista.txtApellidosCliente.setText("");
        vista.txtUsarioCliente.setText("");
        vista.txtContrasenaCliente.setText("");
        vista.spNumeroComprasCliente.setValue(0);
        vista.datePickerInscripcionCliente.setDate(null);
        vista.txtEmailCliente.setText("");

        vista.dlmTiendasCliente.clear();
    }

    /**
     * Método encargado de vaciar los campos del formulario de videojuego
     */
    private void limpiarCamposVideojuego() {
        vista.txtNombreVideojuego.setText("");
        vista.txtGeneroVideojuego.setText("");
        vista.txtPrecioVideojuego.setText("");
        vista.datePickerSalidaVideojuego.setDate(null);
        vista.txtPuntuacionVideojuego.setText("");
        vista.txtPlataformaVideojuego.setText("");
        vista.spEdadMinimaJugarVideojuego.setValue(0);

        vista.dlmTiendasVideojuego.clear();
    }

    /**
     * Método encargado de limpiar los inputs de los formularios de cada objeto y las listas de toda la vista
     */
    private void limpiarDatosVista() {
        vista.dlmTiendas.clear();
        vista.dlmClientesTienda.clear();
        vista.dlmEmpleadosTienda.clear();
        vista.dlmVideojuegosTienda.clear();
        limpiarCamposTienda();

        vista.dlmEmpleados.clear();
        vista.txtTiendaEmpleado.setText("");
        limpiarCamposEmpleado();

        vista.dlmClientes.clear();
        vista.dlmTiendasCliente.clear();
        limpiarCamposCliente();

        vista.dlmVideojuegos.clear();
        vista.dlmTiendasVideojuego.clear();
        limpiarCamposVideojuego();
    }

    // **** MÉTODOS DE VERIFICACIÓN ****

    /**
     * Método encargado de verificar una tienda
     * @param tiendaAVerificar
     * @return
     */
    private boolean verificarTienda(Tienda tiendaAVerificar) {
        boolean verificado = true;

        verificado = verificarDatosTienda(tiendaAVerificar);

        if (verificado == true) {
            verificado = comprobarDatosIrrepetiblesTienda(tiendaAVerificar);
        }

        return verificado;
    }

    /**
     * Método encargado de verificar que todos los campos de una tienda han sido rellenados y se encuentran dentro de los
     * rangos permitidos
     * @param tiendaAVerificar
     * @return
     */
    private boolean verificarDatosTienda(Tienda tiendaAVerificar) {
        boolean verificado = true;

        if (tiendaAVerificar.getNombre().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido el nombre de la tienda");
        }

        if (tiendaAVerificar.getDireccion().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido la dirección de la tienda");
        }

        if (tiendaAVerificar.getTelefono().length() != 9) {
            verificado = false;
            Util.mensajeError("Longitud de teléfono incorrecta");
        }

        if (tiendaAVerificar.getEmail().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido el email de la tienda");
        }

        if (tiendaAVerificar.getNumVentas() < 0) {
            verificado = false;
            Util.mensajeError("El número de ventas no puede ser menor que 0");
        }

        if(tiendaAVerificar.getPuntuacion() < 0 || tiendaAVerificar.getPuntuacion() > 10) {
            verificado = false;
            Util.mensajeError("La puntuación debe ser un número entre 1 y 10");
        }

        return verificado;
    }

    /**
     * Método encargado de comprobar que los datos irrepetibles de una tienda no se encuentran ya en la base de datos
     * @param tiendaAVerificar
     * @return
     */
    private boolean comprobarDatosIrrepetiblesTienda(Tienda tiendaAVerificar) {
        List<Tienda> tiendasAplicacion = modelo.getTiendas();
        boolean verificado = true;

        for(Tienda tienda : tiendasAplicacion) {
            if (tiendaAVerificar.getId() != tienda.getId()) {
                if (tienda.getDireccion().equalsIgnoreCase(tiendaAVerificar.getDireccion())) {
                    verificado = false;
                    Util.mensajeError("Dirección ya existente");
                } else if (tienda.getTelefono().equalsIgnoreCase(tiendaAVerificar.getTelefono())) {
                    verificado = false;
                    Util.mensajeError("Teléfono ya existente");
                } else if (tienda.getEmail().equalsIgnoreCase(tiendaAVerificar.getEmail())) {
                    verificado = false;
                    Util.mensajeError("Email ya existente");
                }
            }
        }

        return verificado;
    }

    /**
     * Método encargado de verificar un Empleado
     * @param empleadoAVerificar
     * @return
     */
    private boolean verificarEmpleado(Empleado empleadoAVerificar) {
        boolean verificado = true;

        verificado = verificarDatosEmpleado(empleadoAVerificar);

        if (verificado == true) {
            verificado = comprobarDatosIrrepetiblesEmpleado(empleadoAVerificar);
        }

        return verificado;
    }

    /**
     * Método encargado de verificar que todos los campos de un empleado han sido rellenados y se encuentran dentro de los
     * rangos permitidos
     * @param empleadoAVerificar
     * @return
     */
    private boolean verificarDatosEmpleado(Empleado empleadoAVerificar) {
        boolean verificado = true;

        if (empleadoAVerificar.getNombre().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido el nombre del empleado");
        }

        if (empleadoAVerificar.getApellidos().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido los apellidos del empleado");
        }

        if (empleadoAVerificar.getTelefono().length() != 9) {
            verificado = false;
            Util.mensajeError("Longitud de teléfono incorrecta");
        }

        if (empleadoAVerificar.getEdad() < 16) {
            verificado = false;
            Util.mensajeError("La edad no puede ser menor de 16 años");
        }

        if (empleadoAVerificar.getEmail().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido el email de la tienda");
        }

        return verificado;
    }

    /**
     * Método encargado de comprobar que los datos irrepetibles de un empledo no se encuentran ya en la base de datos
     * @param empleadoAVerificar
     * @return
     */
    private boolean comprobarDatosIrrepetiblesEmpleado(Empleado empleadoAVerificar) {
        List<Empleado> empleadosAplicacion = modelo.getEmpleados();
        boolean verificado = true;

        for(Empleado empleado : empleadosAplicacion) {
            if (empleadoAVerificar.getId() != empleado.getId()) {
                if (empleado.getDni().equalsIgnoreCase(empleadoAVerificar.getDni())) {
                    verificado = false;
                    Util.mensajeError("DNI ya existente");
                } else if (empleado.getTelefono().equalsIgnoreCase(empleadoAVerificar.getTelefono())) {
                    verificado = false;
                    Util.mensajeError("Teléfono ya existente");
                } else if (empleado.getEmail().equalsIgnoreCase(empleadoAVerificar.getEmail())) {
                    verificado = false;
                    Util.mensajeError("Email ya existente");
                }
            }
        }

        return verificado;
    }

    /**
     * Método encargado de verificar un cliente
     * @param clienteAVerificar
     * @return
     */
    private boolean verificarCliente(Cliente clienteAVerificar) {
        boolean verificado = true;

        verificado = verificarDatosCliente(clienteAVerificar);

        if (verificado == true) {
            verificado = comprobarDatosIrrepetiblesCliente(clienteAVerificar);
        }

        return verificado;
    }

    /**
     * Método encargado de verificar que todos los campos de un cliente han sido rellenados y se encuentran dentro de los
     * rangos permitidos
     * @param clienteAVerificar
     * @return
     */
    private boolean verificarDatosCliente(Cliente clienteAVerificar) {
        boolean verificado = true;

        if (clienteAVerificar.getNombre().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido el nombre del cliente");
        }

        if (clienteAVerificar.getApellidos().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido el apellido del cliente");
        }

        if (clienteAVerificar.getUsuario().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido el usuario del cliente");
        }

        if (clienteAVerificar.getContrasena().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido la contraseña del cliente");
        }

        if (clienteAVerificar.getNumCompras() < 0) {
            verificado = false;
            Util.mensajeError("Las compras de un cliente no pueden ser menos de 0");
        }

        if (clienteAVerificar.getEmail().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido el email del cliente");
        }

        if (clienteAVerificar.getFechaInscripcion() == null) {
            verificado = false;
            Util.mensajeError("Debe introducir la fecha de inscripción del cliente");
        }

        return verificado;
    }

    /**
     * Método encargado de comprobar que los datos irrepetibles de un cliente no se encuentran ya en la base de datos
     * @param clienteAVerificar
     * @return
     */
    private boolean comprobarDatosIrrepetiblesCliente(Cliente clienteAVerificar) {
        List<Cliente> clientesAplicacion = modelo.getClientes();
        boolean verificado = true;

        for(Cliente cliente : clientesAplicacion) {
            if (clienteAVerificar.getId() != cliente.getId()) {
                if (cliente.getUsuario().equalsIgnoreCase(clienteAVerificar.getUsuario())) {
                    verificado = false;
                    Util.mensajeError("Nombre de usuario ya existente");
                } else if (cliente.getEmail().equalsIgnoreCase(clienteAVerificar.getEmail())) {
                    verificado = false;
                    Util.mensajeError("Email ya existente");
                }
            }
        }

        return verificado;
    }

    /**
     * Método encargado de verificar un videojuego
     * @param videojuegoAVerificar
     * @return
     */
    private boolean verificarVideojuego(Videojuego videojuegoAVerificar) {
        boolean verificado = true;

        verificado = verificarDatosVideojuego(videojuegoAVerificar);

        if (verificado == true) {
            verificado = comprobarDatosIrrepetiblesVideojuego(videojuegoAVerificar);
        }

        return verificado;
    }

    /**
     * Método encargado de verificar que todos los campos de un videojuego han sido rellenados y se encuentran dentro de los
     * rangos permitidos
     * @param videojuegoAVerificar
     * @return
     */
    private boolean verificarDatosVideojuego(Videojuego videojuegoAVerificar) {
        boolean verificado = true;

        if (videojuegoAVerificar.getNombre().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido el nombre del videojuego");
        }

        if (videojuegoAVerificar.getGenero().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido el género del videojuego");
        }

        if (videojuegoAVerificar.getPrecio() < 0) {
            verificado = false;
            Util.mensajeError("El precio del videojuego no puede ser menor que 0");
        }

        if (videojuegoAVerificar.getFechaSalida() == null) {
            verificado = false;
            Util.mensajeError("No has introducido la fecha de salida del videojuego");
        }

        if (videojuegoAVerificar.getPuntuacion() < 0) {
            verificado = false;
            Util.mensajeError("La puntuación no puede ser menor que 0");
        }

        if (videojuegoAVerificar.getPlataforma().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("No has introducido la plataforma del videojuego");
        }

        if(videojuegoAVerificar.getEdadMinima() < 0) {
            verificado = false;
            Util.mensajeError("La edad mínima no puede ser menor que 0");
        }

        return verificado;
    }

    /**
     * Método encargado de comprobar que los datos irrepetibles de un videojuego no se encuentran ya en la base de datos
     * @param videojuegoAVerificar
     * @return
     */
    private boolean comprobarDatosIrrepetiblesVideojuego(Videojuego videojuegoAVerificar) {
        List<Videojuego> videojuegosAplicacion = modelo.getVideojuegos();
        boolean verificado = true;

        for(Videojuego videojuego : videojuegosAplicacion) {
            if (videojuegoAVerificar.getId() != videojuego.getId()) {
                if (videojuego.getNombre().equalsIgnoreCase(videojuegoAVerificar.getNombre())) {
                    verificado = false;
                    Util.mensajeError("Nombre ya existente");
                }
            }
        }

        return verificado;
    }

    /**
     * Método encargado de comprobar que el String recibido sea un número entero
     * @param numero
     * @return
     */
    private boolean verificarNumeroEntero(String numero) {
        try {
            Integer.parseInt(numero);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }

    /**
     * Método encargado de comprobar que el String recibido sea un número Float
     * @param numero
     * @return
     */
    private boolean verificarNumeroFlotante(String numero) {
        try {
            Float.parseFloat(numero);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }

    /**
     * Método encargado de comprobar que el String recibido sea un DNI correcto
     * @param dni
     * @return
     */
    private boolean verificarDni(String dni) {
        boolean verificado = true;
        String[] letrasDni = {"T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V",
                "H", "L", "C", "K", "E"};

        if(dni.length() != 9) {
            verificado = false;
            Util.mensajeError("Longitud DNI incorrecta");
        }

        if (verificado) {
            String numerosDni = dni.substring(0, 8);
            verificado = verificarNumeroEntero(numerosDni);

            if (verificado) {
                String letraDni = dni.substring(8).toUpperCase();
                int resto = Integer.parseInt(numerosDni) % 23;

                String resultadoLetra = letrasDni[resto];
                System.out.println(Integer.parseInt(numerosDni));
                System.out.println(resultadoLetra);
                System.out.println(letraDni);

                if(!letraDni.equalsIgnoreCase(resultadoLetra)) {
                    verificado = false;
                    Util.mensajeError("Formato de DNI incorrecto");
                }
            }
            else {
                Util.mensajeError("Formato de DNI incorrecto");
            }
        }


        return verificado;
    }

    /**
     * Método encargado de eliminar todas las relaciones de una tienda
     * @param tiendaAVaciar
     */
    private void vaciarRelacionesTienda(Tienda tiendaAVaciar) {
        for(Empleado empleado : tiendaAVaciar.getEmpleados()) {
            empleado.setTienda(null);
        }

        for(Cliente cliente : tiendaAVaciar.getClientes()) {
            cliente.getTiendas().remove(tiendaAVaciar);
        }

        tiendaAVaciar.setClientes(null);

        for(Videojuego videojuego : tiendaAVaciar.getVideojuegos()) {
            videojuego.getTiendas().remove(tiendaAVaciar);
        }

        tiendaAVaciar.setVideojuegos(null);
    }

    /**
     * Método encargado de dar de alta una tienda
     */
    private void altaTienda() {
        boolean tiendaVerificada = true;
        Tienda nuevaTienda = new Tienda();

        nuevaTienda.setNombre(vista.txtNombreTienda.getText());
        nuevaTienda.setDireccion(vista.txtDireccionTienda.getText());
        nuevaTienda.setEmail(vista.txtEmailTienda.getText());
        nuevaTienda.setNumVentas((Integer) vista.spNumeroVentasTienda.getValue());

        // Se verifica que haya una fecha seleccionada
        if (vista.datePickerFechaAperturaTienda.getDate() != null) {
            nuevaTienda.setFechaApertura(Date.valueOf(vista.datePickerFechaAperturaTienda.getDate()));
        }
        else {
            tiendaVerificada = false;
            Util.mensajeError("Debe introducir una fecha");
        }

        // Se verifica el formato de la puntuación
        if (verificarNumeroFlotante(vista.txtPuntuacionTienda.getText())) {
            nuevaTienda.setPuntuacion(Float.parseFloat(vista.txtPuntuacionTienda.getText()));
        }
        else {
            tiendaVerificada = false;
            Util.mensajeError("Formato de la puntuación incorrecto (Debe se un número)");
        }

        // Se verifica el formato del teléfono
        if (verificarNumeroEntero(vista.txtTelefonoTienda.getText())) {
            nuevaTienda.setTelefono(vista.txtTelefonoTienda.getText());
        }
        else {
            tiendaVerificada = false;
            Util.mensajeError("Formato del teléfono incorrecto (Debe se un número)");
        }



        // Se verifican que los datos no esten vacíos y no esten en la base de datos
        if (tiendaVerificada == true) {
            tiendaVerificada = verificarTienda(nuevaTienda);
        }

        if (tiendaVerificada == true) {
            modelo.guardarTiendaCerrandoSesion(nuevaTienda);
            limpiarCamposTienda();
            Util.mensajeInformativo("Tienda guardada con éxito");
        }
    }

    /**
     * Método encargado de modificar una tienda
     */
    private void modificarTienda() {
        boolean tiendaVerificadaModificar = true;
        Tienda tiendaSeleccionadaModificar = (Tienda) vista.listTiendas.getSelectedValue();

        if (tiendaSeleccionadaModificar != null) {
            tiendaSeleccionadaModificar.setNombre(vista.txtNombreTienda.getText());
            tiendaSeleccionadaModificar.setDireccion(vista.txtDireccionTienda.getText());
            tiendaSeleccionadaModificar.setEmail(vista.txtEmailTienda.getText());
            tiendaSeleccionadaModificar.setNumVentas((Integer) vista.spNumeroVentasTienda.getValue());

            // Se verifica que haya una fecha seleccionada
            if (vista.datePickerFechaAperturaTienda.getDate() != null) {
                tiendaSeleccionadaModificar.setFechaApertura(Date.valueOf(vista.datePickerFechaAperturaTienda.getDate()));
            } else {
                tiendaVerificadaModificar = false;
                Util.mensajeError("Debe introducir una fecha");
            }

            // Se verifica el formato de la puntuación
            if (verificarNumeroFlotante(vista.txtPuntuacionTienda.getText())) {
                tiendaSeleccionadaModificar.setPuntuacion(Float.parseFloat(vista.txtPuntuacionTienda.getText()));
            } else {
                tiendaVerificadaModificar = false;
                Util.mensajeInformativo("Formato de la puntuación incorrecto (Debe se un número)");
            }

            // Se verifica el formato del teléfono
            if (verificarNumeroEntero(vista.txtTelefonoTienda.getText())) {
                tiendaSeleccionadaModificar.setTelefono(vista.txtTelefonoTienda.getText());
            } else {
                tiendaVerificadaModificar = false;
                Util.mensajeError("Formato del teléfono incorrecto (Debe se un número)");
            }

            // Se verifican que los datos no esten vacíos y no esten en la base de datos
            if (tiendaVerificadaModificar == true) {
                tiendaVerificadaModificar = verificarTienda(tiendaSeleccionadaModificar);
            }

            if (tiendaVerificadaModificar == true) {
                modelo.guardarTiendaCerrandoSesion(tiendaSeleccionadaModificar);
                limpiarCamposTienda();
                Util.mensajeInformativo("Tienda modificada con éxito");
            }
        }
        else {
            Util.mensajeError("Debe seleccionar una tienda");
        }
    }

    /**
     * Método encargado de eliminar una tienda
     */
    private void eliminarTienda() {
        Tienda tiendaSeleccionadaEliminar = (Tienda) vista.listTiendas.getSelectedValue();
        if (tiendaSeleccionadaEliminar != null) {
            int resultado = JOptionPane.showConfirmDialog(null, "¿Estás seguro de eliminar la tienda " + tiendaSeleccionadaEliminar.getNombre() + "?", "Confirmar", JOptionPane.YES_NO_OPTION);
            if (resultado == 0) {
                vaciarRelacionesTienda(tiendaSeleccionadaEliminar);
                modelo.eliminarTienda(tiendaSeleccionadaEliminar);
                Util.mensajeInformativo("Tienda eliminada con éxito");
                limpiarCamposTienda();
            }
        }
        else {
            Util.mensajeError("Debe seleccionar una tienda");
        }
    }

    /**
     * Método encargado de dar de alta un empleado
     */
    private void altaEmpleado() {
        boolean empleadoVerificado = true;
        Empleado nuevoEmpleado = new Empleado();

        nuevoEmpleado.setNombre(vista.txtNombreEmpleado.getText());
        nuevoEmpleado.setApellidos(vista.txtApellidosEmpleado.getText());
        nuevoEmpleado.setSexo(vista.cbSexoEmpleado.getSelectedItem().toString());
        nuevoEmpleado.setEdad((Integer) vista.spEdadEmpleado.getValue());
        nuevoEmpleado.setEmail(vista.txtEmailEmpleado.getText());

        // Se verifica que haya una fecha seleccionada
        if (vista.datePickerContratacionEmpleado.getDate() != null) {
            nuevoEmpleado.setFechaContratacion(Date.valueOf(vista.datePickerContratacionEmpleado.getDate()));
        }
        else {
            empleadoVerificado = false;
            Util.mensajeError("Debe introducir una fecha");
        }

        // Se verifica el formato del DNI y que sea correcto
        if(verificarDni(vista.txtDniEmpleado.getText())) {
            nuevoEmpleado.setDni(vista.txtDniEmpleado.getText());
        }
        else {
            empleadoVerificado = false;
        }

        // Se verifica el formato del número de teléfono
        if (verificarNumeroEntero(vista.txtTelefonoEmpleado.getText())) {
            nuevoEmpleado.setTelefono(vista.txtTelefonoEmpleado.getText());
        }
        else {
            empleadoVerificado = false;
            Util.mensajeError("Formato del teléfono incorrecto (Debe se un número)");
        }

        // Se verifican que los datos no esten vacíos y no esten en la base de datos
        if (empleadoVerificado) {
            empleadoVerificado = verificarEmpleado(nuevoEmpleado);
        }

        if (empleadoVerificado) {
            modelo.guardarEmpleadoCerrandoSesion(nuevoEmpleado);
            limpiarCamposEmpleado();
            Util.mensajeInformativo("Empleado guardado con éxito");
        }

    }

    /**
     * Método encargado de modificar un empleado
     */
    private void modificarEmpleado() {
        boolean empleadoVerificadoModificar = true;
        Empleado empleadoSeleccionadoMofificar = (Empleado) vista.listEmpleados.getSelectedValue();

        if (empleadoSeleccionadoMofificar != null) {
            empleadoSeleccionadoMofificar.setNombre(vista.txtNombreEmpleado.getText());
            empleadoSeleccionadoMofificar.setApellidos(vista.txtApellidosEmpleado.getText());
            empleadoSeleccionadoMofificar.setSexo(vista.cbSexoEmpleado.getSelectedItem().toString());
            empleadoSeleccionadoMofificar.setEdad((Integer) vista.spEdadEmpleado.getValue());
            empleadoSeleccionadoMofificar.setEmail(vista.txtEmailEmpleado.getText());

            // Se verifica que haya una fecha seleccionada
            if (vista.datePickerContratacionEmpleado.getDate() != null) {
                empleadoSeleccionadoMofificar.setFechaContratacion(Date.valueOf(vista.datePickerContratacionEmpleado.getDate()));
            } else {
                empleadoVerificadoModificar = false;
                Util.mensajeError("Debe introducir una fecha");
            }

            // Se verifica el formato del DNI y que sea correcto
            if (verificarDni(vista.txtDniEmpleado.getText())) {
                empleadoSeleccionadoMofificar.setDni(vista.txtDniEmpleado.getText());
            } else {
                empleadoVerificadoModificar = false;
            }

            // Se verifica el formato del teléfono
            if (verificarNumeroEntero(vista.txtTelefonoEmpleado.getText())) {
                empleadoSeleccionadoMofificar.setTelefono(vista.txtTelefonoEmpleado.getText());
            } else {
                empleadoVerificadoModificar = false;
                Util.mensajeError("Formato del teléfono incorrecto (Debe se un número)");
            }

            // Se verifican que los datos no esten vacíos y no esten en la base de datos
            if (empleadoVerificadoModificar) {
                empleadoVerificadoModificar = verificarEmpleado(empleadoSeleccionadoMofificar);
            }

            if (empleadoVerificadoModificar) {
                modelo.guardarEmpleadoCerrandoSesion(empleadoSeleccionadoMofificar);
                Util.mensajeInformativo("Empleado modificado con éxito");
                limpiarCamposEmpleado();
            }
        }
        else {
            Util.mensajeError("Debe seleccionar un empleado");
        }
    }

    /**
     * Método encargado de eliminar un empleado
     */
    private void eliminarEmpleado() {
        Empleado empleadoSeleccionadoEliminar = (Empleado) vista.listEmpleados.getSelectedValue();
        if (empleadoSeleccionadoEliminar != null) {
            int resultadoEliminarEmpleado = JOptionPane.showConfirmDialog(null, "¿Estás seguro de eliminar el empleado " + empleadoSeleccionadoEliminar.getNombre() + "?", "Confirmar", JOptionPane.YES_NO_OPTION);
            if (resultadoEliminarEmpleado == 0) {
                modelo.eliminarEmpleado(empleadoSeleccionadoEliminar);
                limpiarCamposEmpleado();
                Util.mensajeInformativo("Empleado eliminado con éxito");
            }
        }
        else {
            Util.mensajeError("Debe seleccionar un empleado");
        }
    }

    /**
     * Método encargado de dar de alta un cliente
     */
    private void altaCliente() {
        boolean clienteVerificado = true;
        Cliente nuevoCliente = new Cliente();

        nuevoCliente.setNombre(vista.txtNombreCliente.getText());
        nuevoCliente.setApellidos(vista.txtApellidosCliente.getText());
        nuevoCliente.setUsuario(vista.txtUsarioCliente.getText());
        nuevoCliente.setContrasena(vista.txtContrasenaCliente.getText());
        nuevoCliente.setNumCompras((Integer) vista.spNumeroComprasCliente.getValue());
        nuevoCliente.setEmail(vista.txtEmailCliente.getText());

        // Se verifica que haya una fecha seleccionada
        if (vista.datePickerInscripcionCliente.getDate() != null) {
            nuevoCliente.setFechaInscripcion(Date.valueOf(vista.datePickerInscripcionCliente.getDate()));
        }
        else {
            clienteVerificado = false;
            Util.mensajeError("Debe introducir una fecha");
        }

        // Se verifican que los datos no esten vacíos y no esten en la base de datos
        clienteVerificado = verificarCliente(nuevoCliente);

        if (clienteVerificado) {
            modelo.guardarClienteCerrandoSesion(nuevoCliente);
            limpiarCamposCliente();
            Util.mensajeInformativo("Cliente guardado con éxito");
        }
    }

    /**
     * Método encargado de modificar un cliente
     */
    private void modificarCliente() {
        boolean clienteVerificadoModificar = true;
        Cliente clienteSeleccionadoModificar = (Cliente) vista.listClientes.getSelectedValue();

        if (clienteSeleccionadoModificar != null) {
            clienteSeleccionadoModificar.setNombre(vista.txtNombreCliente.getText());
            clienteSeleccionadoModificar.setApellidos(vista.txtApellidosCliente.getText());
            clienteSeleccionadoModificar.setUsuario(vista.txtUsarioCliente.getText());
            clienteSeleccionadoModificar.setContrasena(vista.txtContrasenaCliente.getText());
            clienteSeleccionadoModificar.setNumCompras((Integer) vista.spNumeroComprasCliente.getValue());
            clienteSeleccionadoModificar.setEmail(vista.txtEmailCliente.getText());

            // Se verifica que haya una fecha seleccionada
            if (vista.datePickerInscripcionCliente.getDate() != null) {
                clienteSeleccionadoModificar.setFechaInscripcion(Date.valueOf(vista.datePickerInscripcionCliente.getDate()));
            } else {
                clienteVerificadoModificar = false;
                Util.mensajeError("Debe introducir una fecha");
            }

            // Se verifican que los datos no esten vacíos y no esten en la base de datos
            clienteVerificadoModificar = verificarCliente(clienteSeleccionadoModificar);

            if (clienteVerificadoModificar) {
                modelo.guardarClienteCerrandoSesion(clienteSeleccionadoModificar);
                limpiarCamposCliente();
                Util.mensajeInformativo("Cliente modificado con éxito");
            }
        }
        else {
            Util.mensajeError("Debe seleccionar un cliente");
        }
    }

    /**
     * Método encargado de eliminar un cliente
     */
    private void eliminarCliente() {
        Cliente clienteSeleccionadoEliminar = (Cliente) vista.listClientes.getSelectedValue();
        if (clienteSeleccionadoEliminar != null) {
            int resultadoEliminarCliente = JOptionPane.showConfirmDialog(null, "¿Estás seguro de eliminar el cliente " + clienteSeleccionadoEliminar.getNombre() + "?", "Confirmar", JOptionPane.YES_NO_OPTION);
            if (resultadoEliminarCliente == 0) {
                modelo.eliminarCliente(clienteSeleccionadoEliminar);
                limpiarCamposCliente();
                Util.mensajeInformativo("Cliente eliminado con éxito");
            }
        }
        else {
            Util.mensajeError("Debe seleccionar un cliente");
        }
    }

    /**
     * Método encargado de dar de alta un videojuego
     */
    private void altaVideojuego() {
        boolean videojuegoVerificado = true;
        Videojuego nuevoVideojuego = new Videojuego();

        nuevoVideojuego.setNombre(vista.txtNombreVideojuego.getText());
        nuevoVideojuego.setGenero(vista.txtGeneroVideojuego.getText());
        nuevoVideojuego.setPlataforma(vista.txtPlataformaVideojuego.getText());
        nuevoVideojuego.setEdadMinima((Integer) vista.spEdadMinimaJugarVideojuego.getValue());

        // Se verifica que haya una fecha seleccionada
        if (vista.datePickerSalidaVideojuego.getDate() != null) {
            nuevoVideojuego.setFechaSalida(Date.valueOf(vista.datePickerSalidaVideojuego.getDate()));
        }
        else {
            videojuegoVerificado = false;
            Util.mensajeError("Debe introducir una fecha");
        }

        // Se verifica que el formato de precio sea correcto
        if (verificarNumeroFlotante(vista.txtPrecioVideojuego.getText())) {
            nuevoVideojuego.setPrecio(Float.parseFloat(vista.txtPrecioVideojuego.getText()));
        }
        else {
            videojuegoVerificado = false;
            Util.mensajeInformativo("Formato del precio incorrecto (Debe ser un número)");
        }

        // Se verifica que el formato de la puntuación sea correcto
        if (verificarNumeroFlotante(vista.txtPuntuacionVideojuego.getText())) {
            nuevoVideojuego.setPuntuacion(Float.parseFloat(vista.txtPuntuacionVideojuego.getText()));
        }
        else {
            videojuegoVerificado = false;
            Util.mensajeInformativo("Formato de la puntuación incorrecto (Debe ser un número)");
        }

        // Se verifican que los datos no esten vacíos y no esten en la base de datos
        if (videojuegoVerificado) {
            videojuegoVerificado = verificarVideojuego(nuevoVideojuego);
        }

        if (videojuegoVerificado) {
            modelo.guardarVideojuegoCerrandoSesion(nuevoVideojuego);
            limpiarCamposVideojuego();
            Util.mensajeInformativo("Videojuego guardado con éxito");
        }
    }

    /**
     * Método encargado de modificar un videojuego
     */
    private void modificarVideojuego() {
        boolean videojuegoVerificadoModificar = true;
        Videojuego videojuegoSeleccionadoModificar = (Videojuego) vista.listVideojuegos.getSelectedValue();

        if (videojuegoSeleccionadoModificar != null) {
            videojuegoSeleccionadoModificar.setNombre(vista.txtNombreVideojuego.getText());
            videojuegoSeleccionadoModificar.setGenero(vista.txtGeneroVideojuego.getText());
            videojuegoSeleccionadoModificar.setPlataforma(vista.txtPlataformaVideojuego.getText());
            videojuegoSeleccionadoModificar.setEdadMinima((Integer) vista.spEdadMinimaJugarVideojuego.getValue());

            // Se verifica que haya una fecha seleccionada
            if (vista.datePickerSalidaVideojuego.getDate() != null) {
                videojuegoSeleccionadoModificar.setFechaSalida(Date.valueOf(vista.datePickerSalidaVideojuego.getDate()));
            } else {
                videojuegoVerificadoModificar = false;
                Util.mensajeError("Debe introducir una fecha");
            }

            // Se verifica que el formato del precio sea correcto
            if (verificarNumeroFlotante(vista.txtPrecioVideojuego.getText())) {
                videojuegoSeleccionadoModificar.setPrecio(Float.parseFloat(vista.txtPrecioVideojuego.getText()));
            } else {
                videojuegoVerificadoModificar = false;
                Util.mensajeInformativo("Formato del precio incorrecto (Debe ser un número)");
            }

            // Se verifica que el formato de la puntuación sea correcto
            if (verificarNumeroFlotante(vista.txtPuntuacionVideojuego.getText())) {
                videojuegoSeleccionadoModificar.setPuntuacion(Float.parseFloat(vista.txtPuntuacionVideojuego.getText()));
            } else {
                videojuegoVerificadoModificar = false;
                Util.mensajeInformativo("Formato de la puntuación incorrecto (Debe ser un número)");
            }

            // Se verifican que los datos no esten vacíos y no esten en la base de datos
            if (videojuegoVerificadoModificar) {
                videojuegoVerificadoModificar = verificarVideojuego(videojuegoSeleccionadoModificar);
            }

            if (videojuegoVerificadoModificar) {
                modelo.guardarVideojuegoCerrandoSesion(videojuegoSeleccionadoModificar);
                limpiarCamposVideojuego();
                Util.mensajeInformativo("Videojuego modificado con éxito");
            }
        }
        else {
            Util.mensajeError("Debe seleccionar un videojuego");
        }
    }

    /**
     * Método encargado de eliminar un videojuego
     */
    private void eliminarVideojuego() {
        Videojuego videojuegoSeleccionadoEliminar = (Videojuego) vista.listVideojuegos.getSelectedValue();
        if (videojuegoSeleccionadoEliminar != null) {
            int resultadoEliminarVideojuego = JOptionPane.showConfirmDialog(null, "¿Estás seguro de eliminar el videojuego " + videojuegoSeleccionadoEliminar.getNombre() + "?", "Confirmar", JOptionPane.YES_NO_OPTION);
            if (resultadoEliminarVideojuego == 0) {
                modelo.eliminarVideojuego(videojuegoSeleccionadoEliminar);
                limpiarCamposVideojuego();
                Util.mensajeInformativo("Videojuego eliminado con éxito");
            }
        }
        else {
            Util.mensajeError("Debe seleccionar un videojuego");
        }
    }

    /**
     * Método encargado de generar una nueva ventana con el informe de las tiendas de la aplicación
     * @throws JRException
     * @throws SQLException
     */
    public void pintarInformeTiendas() throws JRException, SQLException {
        JasperPrint jasperPrint;

        String url = "jdbc:mysql://127.0.0.1:3306/basevcmanagement?user=root&password=";

        Connection conexionInforme = DriverManager.getConnection(url);

        jasperPrint = JasperFillManager.fillReport("informes/Informe_TiendasVCM.jasper", null, conexionInforme);

        JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);

        jasperViewer.setVisible(true);

        conexionInforme = null;
    }

    /**
     * Método encargado de generar una nueva ventana con el informe de los empleados de la aplicación
     * @throws JRException
     * @throws SQLException
     */
    public void pintarInformeEmpleados() throws JRException, SQLException {
        JasperPrint jasperPrint;

        String url = "jdbc:mysql://127.0.0.1:3306/basevcmanagement?user=root&password=";

        Connection conexionInforme = DriverManager.getConnection(url);

        jasperPrint = JasperFillManager.fillReport("informes/Informe_EmpleadosVCM.jasper", null, conexionInforme);

        JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);

        jasperViewer.setVisible(true);

        conexionInforme = null;
    }

    /**
     * Método encargado de generar una nueva ventana con el informe de los clientes de la aplicación
     * @throws JRException
     * @throws SQLException
     */
    public void pintarInformeClientes() throws JRException, SQLException {
        JasperPrint jasperPrint;

        String url = "jdbc:mysql://127.0.0.1:3306/basevcmanagement?user=root&password=";

        Connection conexionInforme = DriverManager.getConnection(url);

        jasperPrint = JasperFillManager.fillReport("informes/Informe_ClientesVCM.jasper", null, conexionInforme);

        JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);

        jasperViewer.setVisible(true);

        conexionInforme = null;
    }

    /**
     * Método encargado de generar una nueva ventana con el informe de los videojuegos de la aplicación
     * @throws JRException
     * @throws SQLException
     */
    public void pintarInformeVideojuegos() throws JRException, SQLException {
        JasperPrint jasperPrint;

        String url = "jdbc:mysql://127.0.0.1:3306/basevcmanagement?user=root&password=";

        Connection conexionInforme = DriverManager.getConnection(url);

        jasperPrint = JasperFillManager.fillReport("informes/Informe_VideojuegosVCM.jasper", null, conexionInforme);

        JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);

        jasperViewer.setVisible(true);

        conexionInforme = null;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando) {
            case "Conectar":
                vista.itemConectar.setEnabled(false);
                administrarComponentesVista();
                modelo.conectar();
                break;
            case "Administrar usuarios":
                AdministrarUsuariosJDialog administrarUsuariosJDialog = new AdministrarUsuariosJDialog(modelo);
                administrarUsuariosJDialog.mostrarDialogo();
                break;
            case "Alta tienda":
                altaTienda();
                break;
            case "Modificar tienda":
                modificarTienda();
                break;
            case "Eliminar tienda":
                eliminarTienda();
                break;
            case "Alta empleado":
                altaEmpleado();
                break;
            case "Modificar empleado":
                modificarEmpleado();
                break;
            case "Eliminar empleado":
                eliminarEmpleado();
                break;
            case "Alta cliente":
                altaCliente();
                break;
            case "Modificar cliente":
                modificarCliente();
                break;
            case "Eliminar cliente":
                eliminarCliente();
                break;
            case "Alta videojuego":
                altaVideojuego();
                break;
            case "Modificar videojuego":
                modificarVideojuego();
                break;
            case "Eliminar videojuego":
                eliminarVideojuego();
                break;
            case "Administrar empleados":
                Tienda tiendaSeleccionadaAdministrarEmpleado = (Tienda) vista.listTiendas.getSelectedValue();

                if (tiendaSeleccionadaAdministrarEmpleado != null) {
                    AdministrarEmpleadosDialog dialogEmpleados = new AdministrarEmpleadosDialog(tiendaSeleccionadaAdministrarEmpleado, modelo);
                    dialogEmpleados.mostrarDialogo();
                }
                else {
                    Util.mensajeError("Debe seleccionar una tienda");
                }
                break;
            case "Administrar clientes":
                Tienda tiendaSeleccionadaAdministrarClientes = (Tienda) vista.listTiendas.getSelectedValue();

                if (tiendaSeleccionadaAdministrarClientes != null) {
                    AdministrarClientesDialog dialogClientes = new AdministrarClientesDialog(tiendaSeleccionadaAdministrarClientes, modelo);
                    dialogClientes.mostrarDialogo();
                }
                else {
                    Util.mensajeError("Debe seleccionar una tienda");
                }
                break;
            case "Administrar videojuegos":
                Tienda tiendaSelccionadaAdministrarVideojuegos = (Tienda) vista.listTiendas.getSelectedValue();

                if (tiendaSelccionadaAdministrarVideojuegos != null) {
                    AdministrarVideojuegoDialog dialogVideojuegos = new AdministrarVideojuegoDialog(tiendaSelccionadaAdministrarVideojuegos, modelo);
                    dialogVideojuegos.mostrarDialogo();
                }
                else {
                    Util.mensajeError("Debe seleccionar una tienda");
                }
                break;
            case "Limpiar campos tienda":
                limpiarCamposTienda();
                break;
            case "Limpiar campos empleado":
                limpiarCamposEmpleado();
                break;
            case "Limpiar campos cliente":
                limpiarCamposCliente();
                break;
            case "Limpiar datos videojuego":
                limpiarCamposVideojuego();
                break;
            case "Informe tiendas":
                try {
                    pintarInformeTiendas();
                } catch (JRException | SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Informe empleados":
                try {
                    pintarInformeEmpleados();
                } catch (JRException | SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Informe clientes":
                try {
                    pintarInformeClientes();
                } catch (JRException | SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Informe videojuegos":
                try {
                    pintarInformeVideojuegos();
                } catch (JRException | SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Cerrar sesion":
                desactivarVista();

                modelo.desconectar();

                modelo.conectar();

                limpiarDatosVista();

                LoginDialog dialogLogin = new LoginDialog(modelo);
                dialogLogin.mostrarDialogo();

                modelo.desconectar();

                this.rol = dialogLogin.getRol();

                vista.itemConectar.setEnabled(true);
                break;
        }

        listarTodo();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()) {
            if(e.getSource() == vista.listTiendas) {
                Tienda tiendaSeleccionada = (Tienda) vista.listTiendas.getSelectedValue();
                vista.txtNombreTienda.setText(tiendaSeleccionada.getNombre());
                vista.txtDireccionTienda.setText(tiendaSeleccionada.getDireccion());
                vista.datePickerFechaAperturaTienda.setDate(LocalDate.parse(tiendaSeleccionada.getFechaApertura().toString()));
                vista.txtPuntuacionTienda.setText(String.valueOf(tiendaSeleccionada.getPuntuacion()));
                vista.txtTelefonoTienda.setText(tiendaSeleccionada.getTelefono());
                vista.txtEmailTienda.setText(tiendaSeleccionada.getEmail());
                vista.spNumeroVentasTienda.setValue(tiendaSeleccionada.getNumVentas());

                listarEmpleadosTienda(tiendaSeleccionada.getEmpleados());
                listarClientesTienda(tiendaSeleccionada.getClientes());
                listarVideojuegosTienda(tiendaSeleccionada.getVideojuegos());
            }
            else if (e.getSource() == vista.listEmpleados) {
                Empleado empleadoSeleccionado = (Empleado) vista.listEmpleados.getSelectedValue();
                vista.txtNombreEmpleado.setText(empleadoSeleccionado.getNombre());
                vista.txtApellidosEmpleado.setText(empleadoSeleccionado.getApellidos());
                vista.txtDniEmpleado.setText(empleadoSeleccionado.getDni());
                vista.txtTelefonoEmpleado.setText(empleadoSeleccionado.getTelefono());
                if (empleadoSeleccionado.getSexo().equalsIgnoreCase("Hombre")) {
                    vista.cbSexoEmpleado.setSelectedIndex(0);
                }
                else {
                    vista.cbSexoEmpleado.setSelectedIndex(1);

                }
                vista.spEdadEmpleado.setValue(empleadoSeleccionado.getEdad());
                vista.datePickerContratacionEmpleado.setDate(LocalDate.parse(empleadoSeleccionado.getFechaContratacion().toString()));
                vista.txtEmailEmpleado.setText(empleadoSeleccionado.getEmail());
                if (empleadoSeleccionado.getTienda() != null) {
                    vista.txtTiendaEmpleado.setText(empleadoSeleccionado.getTienda().toString());
                }
                else {
                    vista.txtTiendaEmpleado.setText("");

                }
            }
            else if (e.getSource() == vista.listClientes) {
                Cliente clienteSeleccionado = (Cliente) vista.listClientes.getSelectedValue();
                vista.txtNombreCliente.setText(clienteSeleccionado.getNombre());
                vista.txtApellidosCliente.setText(clienteSeleccionado.getApellidos());
                vista.txtUsarioCliente.setText(clienteSeleccionado.getUsuario());
                vista.txtContrasenaCliente.setText(clienteSeleccionado.getContrasena());
                vista.spNumeroComprasCliente.setValue(clienteSeleccionado.getNumCompras());
                vista.datePickerInscripcionCliente.setDate(LocalDate.parse(clienteSeleccionado.getFechaInscripcion().toString()));
                vista.txtEmailCliente.setText(clienteSeleccionado.getEmail());

                listarTiendasCliente(clienteSeleccionado.getTiendas());
            }
            else if (e.getSource() == vista.listVideojuegos) {
                Videojuego videojuegoSeleccionado = (Videojuego) vista.listVideojuegos.getSelectedValue();
                vista.txtNombreVideojuego.setText(videojuegoSeleccionado.getNombre());
                vista.txtGeneroVideojuego.setText(videojuegoSeleccionado.getGenero());
                vista.txtPrecioVideojuego.setText(String.valueOf(videojuegoSeleccionado.getPrecio()));
                vista.datePickerSalidaVideojuego.setDate(LocalDate.parse(videojuegoSeleccionado.getFechaSalida().toString()));
                vista.txtPuntuacionVideojuego.setText(String.valueOf(videojuegoSeleccionado.getPuntuacion()));
                vista.txtPlataformaVideojuego.setText(videojuegoSeleccionado.getPlataforma());
                vista.spEdadMinimaJugarVideojuego.setValue(videojuegoSeleccionado.getEdadMinima());

                listarTiendasVideojuego(videojuegoSeleccionado.getTiendas());
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscarTienda) {
            if (vista.txtBuscarTienda.getText().isEmpty()) {
                listarTiendas(modelo.getTiendas());
            }
            else {
                if(vista.cbBuscarTienda.getSelectedItem().equals("Nombre")) {
                    listarTiendas(modelo.getTiendasBuscar(vista.txtBuscarTienda.getText(), "nombre"));
                }
                else if (vista.cbBuscarTienda.getSelectedItem().equals("Dirección")) {
                    listarTiendas(modelo.getTiendasBuscar(vista.txtBuscarTienda.getText(), "direccion"));
                }
            }
        }
        else if (e.getSource() == vista.txtBuscarEmpleado) {
            if (vista.txtBuscarEmpleado.getText().isEmpty()) {
                listarEmpleados(modelo.getEmpleados());
            }
            else {
                if(vista.cbBuscarEmpleado.getSelectedItem().equals("Nombre")) {
                    listarEmpleados(modelo.getEmpleadosBuscar(vista.txtBuscarEmpleado.getText(), "nombre"));
                }
                else if (vista.cbBuscarEmpleado.getSelectedItem().equals("Apellidos")) {
                    listarEmpleados(modelo.getEmpleadosBuscar(vista.txtBuscarEmpleado.getText(), "apellidos"));
                }
                else if (vista.cbBuscarEmpleado.getSelectedItem().equals("DNI")) {
                    listarEmpleados(modelo.getEmpleadosBuscar(vista.txtBuscarEmpleado.getText(), "dni"));
                }
                else if (vista.cbBuscarEmpleado.getSelectedItem().equals("Sexo")) {
                    listarEmpleados(modelo.getEmpleadosBuscar(vista.txtBuscarEmpleado.getText(), "sexo"));
                }
            }
        }
        else if (e.getSource() == vista.txtBuscarCliente) {
            if (vista.txtBuscarCliente.getText().isEmpty()) {
                listarClientes(modelo.getClientes());
            }
            else {
                if(vista.cbBuscarCliente.getSelectedItem().equals("Nombre")) {
                    listarClientes(modelo.getClientesBuscar(vista.txtBuscarCliente.getText(), "nombre"));
                }
                else if (vista.cbBuscarCliente.getSelectedItem().equals("Apellidos")) {
                    listarClientes(modelo.getClientesBuscar(vista.txtBuscarCliente.getText(), "apellidos"));
                }
                else if (vista.cbBuscarCliente.getSelectedItem().equals("Usuario")) {
                    listarClientes(modelo.getClientesBuscar(vista.txtBuscarCliente.getText(), "usuario"));
                }
            }
        }
        else if (e.getSource() == vista.txtBuscarVideojuego) {
            if (vista.txtBuscarVideojuego.getText().isEmpty()) {
                listarVideojuegos(modelo.getVideojuegos());
            }
            else {
                if(vista.cbBuscarVideojuego.getSelectedItem().equals("Nombre")) {
                    listarVideojuegos(modelo.getVideojuegosBuscar(vista.txtBuscarVideojuego.getText(), "nombre"));
                }
                else if (vista.cbBuscarVideojuego.getSelectedItem().equals("Género")) {
                    listarVideojuegos(modelo.getVideojuegosBuscar(vista.txtBuscarVideojuego.getText(), "genero"));
                }
                else if (vista.cbBuscarVideojuego.getSelectedItem().equals("Precio")) {
                    listarVideojuegos(modelo.getVideojuegosBuscar(vista.txtBuscarVideojuego.getText(), "precio"));
                }
                else if (vista.cbBuscarVideojuego.getSelectedItem().equals("Plataforma")) {
                    listarVideojuegos(modelo.getVideojuegosBuscar(vista.txtBuscarVideojuego.getText(), "plataforma"));
                }
                else if (vista.cbBuscarVideojuego.getSelectedItem().equals("Edad mínima")) {
                    listarVideojuegos(modelo.getVideojuegosBuscar(vista.txtBuscarVideojuego.getText(), "edadMinima"));
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }
}
