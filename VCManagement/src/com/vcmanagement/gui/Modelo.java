package com.vcmanagement.gui;

import com.vcmanagement.base.*;
import com.vcmanagement.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Clase Modelo que se encarga de gestionar toda la información de la aplicación y conectar con la base de datos
 */
public class Modelo {
    SessionFactory sessionFactory;

    /**
     * Método encargado de desconectar con la base de datos
     */
    public void desconectar() {
        HibernateUtil.closeSessionFactory();
    }

    /**
     * Método encargado de conectar con la base de datos
     */
    public void conectar(){
        HibernateUtil.buildSessionFactory();
    }

    /**
     * Método encargado de obtener las Tiendas de la base de datos
     * @return List de objetos Tienda
     */
    public List<Tienda> getTiendas() {
        return (List<Tienda>) HibernateUtil.getCurrentSession().createQuery("FROM Tienda").getResultList();
    }

    /**
     * Método encargado de buscar y obtener las Tiendas cuyos datos coincidan a los pasados por parámetros
     * @param dato String con el dato a buscar en la base de datos
     * @param campo String con el campo donde buscar el dato escrito
     * @return List de objetos Tienda
     */
    public List<Tienda> getTiendasBuscar(String dato, String campo) {
        return (List<Tienda>) HibernateUtil.getCurrentSession().createQuery("FROM Tienda as t WHERE t." + campo + " LIKE '%" + dato + "%'").getResultList();
    }

    /**
     * Método encargado de guardar o sobreescribir una Tienda en la base de datos
     * @param nuevaTienda
     */
    public void guardarTienda(Tienda nuevaTienda) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevaTienda);
        sesion.getTransaction().commit();
    }

    /**
     * Método encargado de guardar o sobreescribir una Tienda en la base de datos y cierra sesión tras hacerlo
     * @param nuevaTienda
     */
    public void guardarTiendaCerrandoSesion(Tienda nuevaTienda) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevaTienda);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método encargado de eliminar una Tienda de la base de datos
     * @param tiendaSeleccionada
     */
    public void eliminarTienda(Tienda tiendaSeleccionada) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(tiendaSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }


    /**
     * Método encargado de obtener los Empleados de la base de datos
     * @return List de objetos Empleado
     */
    public List<Empleado> getEmpleados() {
        return (List<Empleado>) HibernateUtil.getCurrentSession().createQuery("FROM Empleado").getResultList();
    }

    /**
     * Método encargado de buscar y obtener los Empleados cuyos datos coincidan a los pasados por parámetros
     * @param dato String con el dato a buscar en la base de datos
     * @param campo String con el campo donde buscar el dato escrito
     * @return List de objetos Empleado
     */
    public List<Empleado> getEmpleadosBuscar(String dato, String campo) {
        return (List<Empleado>) HibernateUtil.getCurrentSession().createQuery("FROM Empleado as e WHERE e." + campo + " LIKE '%" + dato + "%'").getResultList();
    }

    /**
     * Método encargado de guardar o sobreescribir un Empleado en la base de datos
     * @param nuevoEmpleado
     */
    public void guardarEmpleado(Empleado nuevoEmpleado) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoEmpleado);
        sesion.getTransaction().commit();
    }

    /**
     * Método encargado de guardar o sobreescribir un Empleado en la base de datos y cierra sesión tras hacerlo
     * @param nuevoEmpleado
     */
    public void guardarEmpleadoCerrandoSesion(Empleado nuevoEmpleado) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoEmpleado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método encargado de eliminar un Empleado en la base de datos
     * @param empleadoSeleccionado
     */
    public void eliminarEmpleado(Empleado empleadoSeleccionado) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(empleadoSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método encargado de obtener los Clientes de la base de datos
     * @return List de objetos Cliente
     */
    public List<Cliente> getClientes() {
        return (List<Cliente>) HibernateUtil.getCurrentSession().createQuery("FROM Cliente").getResultList();
    }

    /**
     * Método encargado de buscar y obtener los Clientes cuyos datos coincidan a los pasados por parámetros
     * @param dato String con el dato a buscar en la base de datos
     * @param campo String con el campo donde buscar el dato escrito
     * @return List de objetos Cliente
     */
    public List<Cliente> getClientesBuscar(String dato, String campo) {
        return (List<Cliente>) HibernateUtil.getCurrentSession().createQuery("FROM Cliente as c WHERE c." + campo + " LIKE '%" + dato + "%'").getResultList();
    }

    /**
     * Método encargado de guardar o sobreescribir un Cliente en la base de datos
     * @param nuevoCliente
     */
    public void guardarCliente(Cliente nuevoCliente) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoCliente);
        sesion.getTransaction().commit();
    }

    /**
     * Método encargado de guardar o sobreescribir un Empleado en la base de datos y cerrar sesión tras hacerlo
     * @param nuevoCliente
     */
    public void guardarClienteCerrandoSesion(Cliente nuevoCliente) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoCliente);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método encargado de eliminar un Empleado de la base de datos
     * @param clienteSeleccionado
     */
    public void eliminarCliente(Cliente clienteSeleccionado) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(clienteSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método encargado de obtener los Videojuegos de la base de datos
     * @return List de objetos Videojuego
     */
    public List<Videojuego> getVideojuegos() {
        return (List<Videojuego>) HibernateUtil.getCurrentSession().createQuery("FROM Videojuego ").getResultList();
    }

    /**
     * Método encargado de buscar y obtener los Videojuegos cuyos datos coincidan a los pasados por parámetros
     * @param dato String con el dato a buscar en la base de datos
     * @param campo String con el campo donde buscar el dato escrito
     * @return List de objetos Videojuego
     */
    public List<Videojuego> getVideojuegosBuscar(String dato, String campo) {
        return (List<Videojuego>) HibernateUtil.getCurrentSession().createQuery("FROM Videojuego as v WHERE v." + campo + " LIKE '%" + dato + "%'").getResultList();
    }

    /**
     * Método encargado de guardar o sobreescribir un Videojuego en la base de datos
     * @param nuevoVideojuego
     */
    public void guardarVideojuego(Videojuego nuevoVideojuego) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoVideojuego);
        sesion.getTransaction().commit();
    }

    /**
     * Método encargado de guardar o sobreescribir un Videojuego en la base de datos y cerrar sesión tras hacerlo
     * @param nuevoVideojuego
     */
    public void guardarVideojuegoCerrandoSesion(Videojuego nuevoVideojuego) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoVideojuego);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método encargado de eliminar un Videojuego de la base de datos
     * @param videojuegoSeleccionado
     */
    public void eliminarVideojuego(Videojuego videojuegoSeleccionado) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(videojuegoSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método encargado de obtener los Usuarios de la base de datos
     * @return List de objetos Usuario
     */
    public List<Usuario> getUsuarios() {
        return (List<Usuario>) HibernateUtil.getCurrentSession().createQuery("FROM Usuario").getResultList();

    }

    /**
     * Método encargado de buscar y obtener los Usuarios cuyos datos coincidan a los pasados por parámetros
     * @param dato String con el dato a buscar en la base de datos
     * @param campo String con el campo donde buscar el dato escrito
     * @return List de objetos Usuario
     */
    public List<Usuario> getUsuariosBuscar(String dato, String campo) {
        return (List<Usuario>) HibernateUtil.getCurrentSession().createQuery("FROM Usuario as u WHERE u." + campo + " LIKE '%" + dato + "%'").getResultList();
    }

    /**
     * Método encargado de guardar o sobreescribir un Videojuego en la base de datos
     * @param nuevoUsuario
     */
    public void guardarUsuario(Usuario nuevoUsuario) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoUsuario);
        sesion.getTransaction().commit();
    }

    /**
     * Método encargado de guardar o sobreescribir un Videojuego en la base de datos y cerrar sesión tras hacerlo
     * @param nuevoUsuario
     */
    public void guardarUsuarioCerrandoSesion(Usuario nuevoUsuario) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoUsuario);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método encargado de eliminar un Usuario de la base de datos
     * @param usuarioSeleccionado
     */
    public void eliminarUsuario(Usuario usuarioSeleccionado) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(usuarioSeleccionado);
        sesion.getTransaction().commit();
    }

}