package com.vcmanagement.gui;

import com.vcmanagement.base.Cliente;
import com.vcmanagement.base.Tienda;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Clase AdministrarClientesDialog que extiende JDialog y se encarga de crear un JDialog
 * para gestionar los clientes de una tienda
 */
public class AdministrarClientesDialog extends JDialog {
    private JPanel contentPane;
    private JList listClientesTienda;
    private JList listClientesExistentes;
    private JButton btnEliminarClienteTienda;
    private JButton btnAnadirClienteTienda;
    private JButton botonOk;

    private Modelo modelo;
    private Tienda tienda;

    private DefaultListModel<Cliente> dlmClientesTienda;
    private DefaultListModel<Cliente> dlmClientesExistentes;

    /**
     * Constructor de la clase AdministrarClientesDialog que recibe una tienda y el modelo. Crea la interfaz gráfica,
     * asigna los Listeners y los DefaultListModels a las listas
     * @param tienda Objeto Tienda a administrar
     * @param modelo
     */
    public AdministrarClientesDialog(Tienda tienda, Modelo modelo) {
        this.tienda = tienda;
        this.modelo = modelo;

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(botonOk);
        setLocationRelativeTo(null);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();

        anadirListeners();

        iniciarListas();
        setTitle("Tienda: " + tienda.getNombre());

    }

    /**
     * Método encargado de añadir los listeners y gestionar las acciones de cada botón
     */
    public void anadirListeners() {
        botonOk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        btnEliminarClienteTienda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Cliente clienteSeleccionado = (Cliente) listClientesTienda.getSelectedValue();

                if (clienteSeleccionado == null) {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un cliente",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
                else {
                    tienda.getClientes().remove(clienteSeleccionado);
                    clienteSeleccionado.getTiendas().remove(tienda);

                    modelo.guardarCliente(clienteSeleccionado);
                    modelo.guardarTienda(tienda);

                    listarDatos();
                }
            }
        });

        btnAnadirClienteTienda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Cliente clienteSeleccionado = (Cliente) listClientesExistentes.getSelectedValue();

                if (clienteSeleccionado == null) {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un cliente",
                            "Error", JOptionPane.ERROR_MESSAGE);                }
                else {
                    tienda.getClientes().add(clienteSeleccionado);
                    clienteSeleccionado.getTiendas().add(tienda);

                    modelo.guardarCliente(clienteSeleccionado);
                    modelo.guardarTienda(tienda);

                    listarDatos();
                }
            }
        });
    }

    /**
     * Método que cierra el JDialog
     */
    private void onOK() {
        dispose();
    }

    /**
     * Método que lista los empleados de una tienda y de la aplicación en los Jlists del JDialog y lo hace visible
     */
    public void mostrarDialogo() {
        listarDatos();
        setVisible(true);
    }

    /**
     * Método que inicializa los DefaultListModels y se encarga de asignarlos a sus respectivos Jlists
     */
    public void iniciarListas() {
        dlmClientesTienda = new DefaultListModel<>();
        listClientesTienda.setModel(dlmClientesTienda);

        dlmClientesExistentes = new DefaultListModel<>();
        listClientesExistentes.setModel(dlmClientesExistentes);

    }

    /**
     * Método encargado de llamar a todos los métodos para listar
     */
    public void listarDatos() {
        listarClientesExistentes(modelo.getClientes());
        listarClientesTienda();
    }

    /**
     * Método encargado de listar los clientes de una tienda
     */
    public void listarClientesTienda() {
        dlmClientesTienda.clear();
        for (Cliente cliente : tienda.getClientes()) {
            dlmClientesTienda.addElement(cliente);
        }
    }

    /**
     * Método encargado de listar los clientes de la aplicación que no estén asignados a ninguna tienda
     * @param clientes
     */
    private void listarClientesExistentes(List<Cliente> clientes) {
        dlmClientesExistentes.clear();

        List<Cliente> clientesTienda = tienda.getClientes();
        boolean encontrado = false;

        for(Cliente cliente : clientes){
            encontrado = false;
            for (Cliente clienteTienda : clientesTienda) {
                if(cliente.getId() == clienteTienda.getId()) {
                    encontrado = true;
                }
            }
            if (!encontrado) {
                dlmClientesExistentes.addElement(cliente);
            }
        }
    }
}
