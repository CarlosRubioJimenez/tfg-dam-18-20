package com.vcmanagement.gui;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * Clase Vista, encargada de generar la interfaz gráfica de la aplicación
 */
public class Vista {
    JPanel panel1;
    JFrame frame;
    JTabbedPane tabbedPane1;

    // Componentes de la pestaña Tiendas
    JTextField txtNombreTienda;
    JTextField txtDireccionTienda;
    DatePicker datePickerFechaAperturaTienda;
    JTextField txtPuntuacionTienda;
    JTextField txtTelefonoTienda;
    JTextField txtEmailTienda;
    JSpinner spNumeroVentasTienda;
    JList listTiendas;
    JButton btnAltaTienda;
    JButton btnEliminarTienda;
    JButton btnModificarTienda;
    JTextField txtBuscarTienda;
    JList listVideojuegosTienda;
    JList listClientesTienda;
    JList listEmpleadosTienda;
    JButton btnAdministrarEmpleadosTienda;
    JButton btnAdministrarClientesTienda;
    JButton btnAdministrarVideojuegosTienda;
    JComboBox cbBuscarTienda;
    JButton btnLimpiarCamposTienda;
    JButton btnInformeTiendas;

    // Componentes de la pestaña Empleados
    JTextField txtNombreEmpleado;
    JTextField txtApellidosEmpleado;
    JTextField txtDniEmpleado;
    JTextField txtTelefonoEmpleado;
    JComboBox cbSexoEmpleado;
    JSpinner spEdadEmpleado;
    JTextField txtEmailEmpleado;
    DatePicker datePickerContratacionEmpleado;
    JTextField txtBuscarEmpleado;
    JButton btnAltaEmpleado;
    JButton btnModificarEmpleado;
    JButton btnEliminarEmpleado;
    JList listEmpleados;
    JTextField txtTiendaEmpleado;
    JComboBox cbBuscarEmpleado;
    JButton btnLimpiarCamposEmpleado;
    JButton btnInformeEmpleados;

    // Componentes de la pestaña Clientes
    JTextField txtNombreCliente;
    JTextField txtApellidosCliente;
    JTextField txtUsarioCliente;
    JPasswordField txtContrasenaCliente;
    JSpinner spNumeroComprasCliente;
    DatePicker datePickerInscripcionCliente;
    JTextField txtEmailCliente;
    JList listTiendasCliente;
    JButton btnAltaCliente;
    JButton btnModificarCliente;
    JButton btnEliminarCliente;
    JList listClientes;
    JTextField txtBuscarCliente;
    JComboBox cbBuscarCliente;
    JButton btnLimpiarCamposCliente;
    JButton btnInformeClientes;

    // Componentes de la pestaña Videojuego
    JTextField txtNombreVideojuego;
    JTextField txtGeneroVideojuego;
    JTextField txtPrecioVideojuego;
    DatePicker datePickerSalidaVideojuego;
    JTextField txtPuntuacionVideojuego;
    JTextField txtPlataformaVideojuego;
    JSpinner spEdadMinimaJugarVideojuego;
    JList listVideojuegos;
    JTextField txtBuscarVideojuego;
    JList listTiendaVideojuego;
    JButton btnAltaVideojuego;
    JButton btnEliminarVideojuego;
    JButton btnModificarVideojuego;
    JComboBox cbBuscarVideojuego;
    JButton btnLimpiarCamposVideojuego;
    JButton btnInformeVideojuegos;

    //DefaultListModels
    DefaultListModel dlmTiendas;
    DefaultListModel dlmEmpleadosTienda;
    DefaultListModel dlmClientesTienda;
    DefaultListModel dlmVideojuegosTienda;
    DefaultListModel dlmEmpleados;
    DefaultListModel dlmClientes;
    DefaultListModel dlmTiendasCliente;
    DefaultListModel dlmVideojuegos;
    DefaultListModel dlmTiendasVideojuego;

    //Menú
    JMenuItem itemConectar;
    JMenuItem itemAdministrarUsuarios;
    JMenuItem itemCerrarSesion;

    /**
     * Constructor de la clase Vista encargado de que se visualice la interfaz gráfica
     */
    public Vista() {
        frame = new JFrame("VCManagement");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        crearMenu();
        rellenarComboBoxes();
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setSize(1000, 600);
        frame.setVisible(true);

        crearModelos();
    }

    /**
     * Método encargado de crear y asignar los DefaultListModels a las listas de la aplicación
     */
    private void crearModelos() {
        //Modelos del apartado Tienda
        dlmTiendas = new DefaultListModel();
        listTiendas.setModel(dlmTiendas);
        dlmEmpleadosTienda = new DefaultListModel();
        listEmpleadosTienda.setModel(dlmEmpleadosTienda);
        dlmClientesTienda = new DefaultListModel();
        listClientesTienda.setModel(dlmClientesTienda);
        dlmVideojuegosTienda = new DefaultListModel();
        listVideojuegosTienda.setModel(dlmVideojuegosTienda);

        //Modelos del apartado empleados
        dlmEmpleados = new DefaultListModel();
        listEmpleados.setModel(dlmEmpleados);

        //Modelos del apartado clientes
        dlmClientes = new DefaultListModel();
        listClientes.setModel(dlmClientes);
        dlmTiendasCliente = new DefaultListModel();
        listTiendasCliente.setModel(dlmTiendasCliente);

        //Modelos del apartado videojuego
        dlmVideojuegos = new DefaultListModel();
        listVideojuegos.setModel(dlmVideojuegos);
        dlmTiendasVideojuego = new DefaultListModel();
        listTiendaVideojuego.setModel(dlmTiendasVideojuego);
    }

    /**
     * Método encargado de rellenar las opciones de los combo boxes de la aplicación
     */
    private void rellenarComboBoxes() {

        // Tienda
        cbBuscarTienda.addItem("Nombre");
        cbBuscarTienda.addItem("Dirección");

        // Empleado
        cbSexoEmpleado.addItem("Hombre");
        cbSexoEmpleado.addItem("Mujer");
        cbBuscarEmpleado.addItem("Nombre");
        cbBuscarEmpleado.addItem("Apellidos");
        cbBuscarEmpleado.addItem("DNI");
        cbBuscarEmpleado.addItem("Sexo");

        // Cliente
        cbBuscarCliente.addItem("Nombre");
        cbBuscarCliente.addItem("Apellidos");
        cbBuscarCliente.addItem("Usuario");

        // Videojuego
        cbBuscarVideojuego.addItem("Nombre");
        cbBuscarVideojuego.addItem("Género");
        cbBuscarVideojuego.addItem("Precio");
        cbBuscarVideojuego.addItem("Plataforma");
        cbBuscarVideojuego.addItem("Edad mínima");
    }

    /**
     * Método encargado de crear el menú con todas sus opciones
     */
    private void crearMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Opciones");

        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");

        itemAdministrarUsuarios = new JMenuItem("Administrar usuarios");
        itemAdministrarUsuarios.setActionCommand("Administrar usuarios");

        itemCerrarSesion = new JMenuItem("Cerrar sesión");
        itemCerrarSesion.setActionCommand("Cerrar sesion");

        menu.add(itemConectar);
        menu.add(itemAdministrarUsuarios);
        menu.add(itemCerrarSesion);
        menuBar.add(menu);

        frame.setJMenuBar(menuBar);
    }
}
