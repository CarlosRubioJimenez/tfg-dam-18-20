package com.vcmanagement.gui;

import com.vcmanagement.base.Usuario;
import com.vcmanagement.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

/**
 * Clase AdministrarUsuariosJDialog que extiende JDialog y se encarga de crear un JDialog
 * para gestionar los usuarios de la aplicación
 */
public class AdministrarUsuariosJDialog extends JDialog {
    private JComboBox cbRolUsuarios;
    private JTextField txtUsuario;
    private JPasswordField txtContrasena;
    private JList listUsuarios;
    private JButton btnEliminarUsuario;
    private JButton btnAnadirUsuario;
    private JButton btnModificarUsuario;
    private JButton btnOk;
    private JPanel contentPane;
    private JComboBox cbBuscarUsuario;
    private JTextField txtBuscarUsuario;

    private Modelo modelo;

    private DefaultListModel<Usuario> dlmUsuarios;

    /**
     * Constructor de la clase AdministrarUsuariosJDialog que el modelo. Crea la interfaz gráfica,
     * asigna los Listeners, los datos del combo box de los roles y los DefaultListModels a las listas
     * @param modelo
     */
    public AdministrarUsuariosJDialog(Modelo modelo) {

        this.modelo = modelo;

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btnOk);
        setLocationRelativeTo(null);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();

        anadirListeners();
        rellenarComboBox();

        iniciarLista();
        setTitle("Administrar usuarios");

    }

    /**
     * Método encargado de añadir los listeners y gestionar las acciones de cada botón, de la lista y del campos de
     * texto para buscar Usuario
     */
    private void anadirListeners() {

        btnAnadirUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Usuario nuevoUsuario = new Usuario();
                nuevoUsuario.setUsuario(txtUsuario.getText());
                nuevoUsuario.setContrasena(txtContrasena.getText());
                nuevoUsuario.setRol(cbRolUsuarios.getSelectedItem().toString());

                if(verificarUsuario(nuevoUsuario)) {
                    modelo.guardarUsuarioCerrandoSesion(nuevoUsuario);
                    listarUsuarios(modelo.getUsuarios());
                    limpiarCamposUsuario();
                    Util.mensajeInformativo("Usuario creado con éxito");
                }
            }
        });

        btnModificarUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Usuario usuarioSeleccionadoModificar = (Usuario) listUsuarios.getSelectedValue();

                if (usuarioSeleccionadoModificar != null) {
                    usuarioSeleccionadoModificar.setUsuario(txtUsuario.getText());
                    usuarioSeleccionadoModificar.setContrasena(txtContrasena.getText());
                    usuarioSeleccionadoModificar.setRol(cbRolUsuarios.getSelectedItem().toString());

                    if (verificarUsuario(usuarioSeleccionadoModificar)) {
                        modelo.guardarUsuarioCerrandoSesion(usuarioSeleccionadoModificar);
                        listarUsuarios(modelo.getUsuarios());
                        limpiarCamposUsuario();
                        Util.mensajeInformativo("Usuario modificado con éxito");
                    }
                }
                else {
                    Util.mensajeError("Debe seleccionar un usuario");
                }
            }
        });

        btnEliminarUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Usuario usuarioSeleccionadoEliminar = (Usuario) listUsuarios.getSelectedValue();

                if (usuarioSeleccionadoEliminar != null) {
                    int resultadoEliminar = JOptionPane.showConfirmDialog(null, "¿Estás seguro de eliminar el usuario " + usuarioSeleccionadoEliminar.getUsuario() + "?", "Confirmar", JOptionPane.YES_NO_OPTION);
                    if (resultadoEliminar == 0) {
                        modelo.eliminarUsuario(usuarioSeleccionadoEliminar);
                        limpiarCamposUsuario();
                        Util.mensajeInformativo("Usuario eliminado con éxito");
                    }

                    listarUsuarios(modelo.getUsuarios());
                }
                else {
                    Util.mensajeError("Debe seleccionar un usuario");
                }
            }
        });

        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        listUsuarios.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(e.getValueIsAdjusting()) {
                    if(e.getSource() == listUsuarios) {
                        Usuario usuarioSeleccionado = (Usuario) listUsuarios.getSelectedValue();
                        txtUsuario.setText(usuarioSeleccionado.getUsuario());
                        txtContrasena.setText(usuarioSeleccionado.getContrasena());

                        if(usuarioSeleccionado.getRol().equalsIgnoreCase("Administrador")) {
                            cbRolUsuarios.setSelectedIndex(0);
                        }
                        else if(usuarioSeleccionado.getRol().equalsIgnoreCase("Editor")) {
                            cbRolUsuarios.setSelectedIndex(1);
                        }
                        else if(usuarioSeleccionado.getRol().equalsIgnoreCase("Consultor")) {
                            cbRolUsuarios.setSelectedIndex(2);
                        }

                    }
                }
            }
        });

        txtBuscarUsuario.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (txtBuscarUsuario.getText().isEmpty()) {
                    listarUsuarios(modelo.getUsuarios());
                }
                else {
                    if(cbBuscarUsuario.getSelectedItem().equals("Usuario")) {
                        listarUsuarios(modelo.getUsuariosBuscar(txtBuscarUsuario.getText(), "usuario"));
                    }
                    else if (cbBuscarUsuario.getSelectedItem().equals("Rol")) {
                        listarUsuarios(modelo.getUsuariosBuscar(txtBuscarUsuario.getText(), "rol"));
                    }
                }
            }
        });
    }

    /**
     * Método encargado de llamar a todos los métodos para listar
     */
    private void limpiarCamposUsuario() {
        txtUsuario.setText("");
        txtContrasena.setText("");
    }

    /**
     * Método encargado de verificar un Usuario
     * @param usuarioAVerificar
     * @return
     */
    private boolean verificarUsuario(Usuario usuarioAVerificar) {
        boolean verificado = true;

        if (usuarioAVerificar.getUsuario().equalsIgnoreCase("")) {
            verificado = false;
            Util.mensajeError("Debe rellenar el usuario");
        }

        if (verificado) {
            if (usuarioAVerificar.getContrasena().equalsIgnoreCase("")) {
                verificado = false;
                Util.mensajeError("Debe rellenar la contraseña");
            }

            if (verificado) {
                for (Usuario usuario : modelo.getUsuarios()) {
                    if(usuario.getId() != usuarioAVerificar.getId()) {
                        if (usuario.getUsuario().equalsIgnoreCase(usuarioAVerificar.getUsuario())) {
                            verificado = false;
                            Util.mensajeError("Usuario ya existente");
                        }
                    }
                }
            }
        }

        return verificado;
    }

    /**
     * Método encargado de rellenar los combo boxes del JDialog
     */
    private void rellenarComboBox() {
        cbRolUsuarios.addItem("Administrador");
        cbRolUsuarios.addItem("Editor");
        cbRolUsuarios.addItem("Consultor");

        cbBuscarUsuario.addItem("Usuario");
        cbBuscarUsuario.addItem("Rol");
    }

    /**
     * Método encargado de asignar el DefaulListModel a la lista del JDialog
     */
    private void iniciarLista() {
        dlmUsuarios = new DefaultListModel<>();
        listUsuarios.setModel(dlmUsuarios);
    }

    /**
     * Método encargado de listar los usuarios de la aplicación
     * @param usuarios
     */
    private void listarUsuarios(List<Usuario> usuarios) {
        dlmUsuarios.clear();
        for(Usuario usuario : usuarios){
            dlmUsuarios.addElement(usuario);
        }
    }

    /**
     * Método que cierra el JDialog
     */
    private void onOK() {
        dispose();
    }

    /**
     * Método que lista los empleados de una tienda y de la aplicación en los Jlists del JDialog y lo hace visible
     */
    public void mostrarDialogo() {
        listarUsuarios(modelo.getUsuarios());
        setVisible(true);
    }

}
