package com.vcmanagement.gui;

import com.vcmanagement.base.Empleado;
import com.vcmanagement.base.Tienda;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Clase AdministrarEmpleadosDialog que extiende JDialog y se encarga de crear un JDialog
 * para gestionar los empleados de una tienda
 */
public class AdministrarEmpleadosDialog extends JDialog{
    private JList listEmpleadosTienda;
    private JList listEmpleadosExistentes;
    private JButton btnAnadirEmpleadoTienda;
    private JButton btnEliminarEmpleadoTienda;
    private JPanel panel;
    private JButton botonOk;
    private JPanel contentPane;

    private Modelo modelo;
    private Tienda tienda;

    private DefaultListModel<Empleado> dlmEmpleadosTienda;
    private DefaultListModel<Empleado> dlmEmpleadosExistentes;

    /**
     * Constructor de la clase AdministrarClientesDialog que recibe una tienda y el modelo. Crea la interfaz gráfica,
     * asigna los Listeners y los DefaultListModels a las listas
     * @param tienda Objeto Tienda a administrar
     * @param modelo
     */
    public AdministrarEmpleadosDialog(Tienda tienda, Modelo modelo) {
        this.modelo = modelo;
        this.tienda = tienda;

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(botonOk);
        setLocationRelativeTo(null);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();

        anadirListeners();

        iniciarListas();
        setTitle("Tienda: " + tienda.getNombre());
    }

    /**
     * Método encargado de añadir los listeners y gestionar las acciones de cada botón
     */
    public void anadirListeners() {
        botonOk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        btnEliminarEmpleadoTienda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Empleado empleadoSeleccionado = (Empleado) listEmpleadosTienda.getSelectedValue();
                if (empleadoSeleccionado == null) {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un empleado",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
                else {
                    empleadoSeleccionado.setTienda(null);
                    tienda.getEmpleados().remove(empleadoSeleccionado);

                    modelo.guardarEmpleado(empleadoSeleccionado);
                    modelo.guardarTienda(tienda);

                    listarDatos();
                }
            }
        });

        btnAnadirEmpleadoTienda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Empleado empleadoSeleccionado = (Empleado) listEmpleadosExistentes.getSelectedValue();

                if (empleadoSeleccionado == null) {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un empleado",
                            "Error", JOptionPane.ERROR_MESSAGE);                }
                else {
                    empleadoSeleccionado.setTienda(tienda);
                    tienda.getEmpleados().add(empleadoSeleccionado);

                    modelo.guardarTienda(tienda);
                    modelo.guardarEmpleado(empleadoSeleccionado);

                    listarDatos();
                }
            }
        });
    }

    /**
     * Método que cierra el JDialog
     */
    private void onOK() {
        dispose();
    }

    /**
     * Método que lista los empleados de una tienda y de la aplicación en los Jlists del JDialog y lo hace visible
     */
    public void mostrarDialogo() {
        listarDatos();
        setVisible(true);
    }

    /**
     * Método que inicializa los DefaultListModels y se encarga de asignarlos a sus respectivos Jlists
     */
    public void iniciarListas() {
        dlmEmpleadosTienda = new DefaultListModel<>();
        listEmpleadosTienda.setModel(dlmEmpleadosTienda);

        dlmEmpleadosExistentes = new DefaultListModel<>();
        listEmpleadosExistentes.setModel(dlmEmpleadosExistentes);

    }

    /**
     * Método encargado de llamar a todos los métodos para listar
     */
    public void listarDatos() {
        listarEmpleadosExistentes(modelo.getEmpleados());
        listarEmpleadosTienda();
    }

    /**
     * Método encargado de listar los empleados de una tienda
     */
    public void listarEmpleadosTienda() {
        dlmEmpleadosTienda.clear();
        for (Empleado empleado : tienda.getEmpleados()) {
            dlmEmpleadosTienda.addElement(empleado);
        }
    }

    /**
     * Método encargado de listar los empleados de la aplicación que no estén asignados a ninguna tienda
     * @param empleados
     */
    private void listarEmpleadosExistentes(List<Empleado> empleados) {
        dlmEmpleadosExistentes.clear();
        for(Empleado empleado : empleados){
            if (empleado.getTienda() == null) {
                dlmEmpleadosExistentes.addElement(empleado);
            }
        }
    }
}
