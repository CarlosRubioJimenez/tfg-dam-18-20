package com.vcmanagement.gui;

import com.vcmanagement.base.Cliente;
import com.vcmanagement.base.Empleado;
import com.vcmanagement.base.Tienda;
import com.vcmanagement.base.Videojuego;
import com.vcmanagement.util.HibernateUtil;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Date;
import java.util.List;

/**
 * Clase encargada de llevar a cabo las pruebas unitarias de la aplicación
 */
public class PruebasUnitarias {
    private static Modelo modelo;
    private static Tienda tiendaAProbar;
    private static Empleado empleadoAProbar;
    private static Cliente clienteAProbar;
    private static Videojuego videojuegoAProbar;

    @BeforeClass
    /**
     * Método encargado de preparar los objetos a utilizar en las pruebas
     */
    public static void setUpClass() {
        modelo = new Modelo();

        // Preparar tienda
        tiendaAProbar = new Tienda();
        tiendaAProbar.setNombre("TiendaAProbar");
        tiendaAProbar.setDireccion("DirTiendaAProbar");
        tiendaAProbar.setFechaApertura(Date.valueOf("2019-02-19"));
        tiendaAProbar.setPuntuacion(1);
        tiendaAProbar.setTelefono("601064936");
        tiendaAProbar.setEmail("tiendaProbar@email.com");
        tiendaAProbar.setNumVentas(20);

        // Preparar empleado
        empleadoAProbar = new Empleado();
        empleadoAProbar.setNombre("EmpleadoPrueba");
        empleadoAProbar.setApellidos("García");
        empleadoAProbar.setFechaContratacion(Date.valueOf("2019-02-19"));
        empleadoAProbar.setEmail("juanPrueba@gmail.com");
        empleadoAProbar.setDni("22034511M");
        empleadoAProbar.setEdad(34);
        empleadoAProbar.setTelefono("321456789");
        empleadoAProbar.setSexo("Hombre");

        // Preparar cliente
        clienteAProbar = new Cliente();
        clienteAProbar.setNombre("ClientePrueba");
        clienteAProbar.setApellidos("Pérez");
        clienteAProbar.setUsuario("jppz");
        clienteAProbar.setContrasena("123456");
        clienteAProbar.setNumCompras(7);
        clienteAProbar.setFechaInscripcion(Date.valueOf("2019-02-19"));
        clienteAProbar.setEmail("joseperez@gmail.com");

        // Preparar videojuego
        videojuegoAProbar = new Videojuego();
        videojuegoAProbar.setNombre("VideojuegoPrueba");
        videojuegoAProbar.setGenero("Peleas");
        videojuegoAProbar.setPrecio(49);
        videojuegoAProbar.setFechaSalida(Date.valueOf("2019-02-19"));
        videojuegoAProbar.setPuntuacion(7);
        videojuegoAProbar.setPlataforma("Playstation 3 y 4");
        videojuegoAProbar.setEdadMinima(18);

    }

    @Test
    /**
     * Método encargado de probar la conexión con la base de datos
     */
    public void conectarTest() {
        HibernateUtil.buildSessionFactory();
        HibernateUtil.openSession();

        Assert.assertTrue(HibernateUtil.getSession().isOpen());
    }

    @Test
    /**
     * Método encargado de probar el funcionamiento de guardar una tienda
     */
    public void guardarTiendaTest() {
        HibernateUtil.buildSessionFactory();

        List listaTiendas = modelo.getTiendas();
        int numeroTiendasInicial = listaTiendas.size();

        modelo.guardarTienda(this.tiendaAProbar);
        listaTiendas = modelo.getTiendas();
        int numeroTiendasActual = listaTiendas.size();

        Assert.assertEquals(numeroTiendasInicial + 1, numeroTiendasActual);
    }

    @Test
    /**
     * Método encargado de probar el funcionamiento de guardar un empleado
     */
    public void guardarEmpleadoTest() {
        HibernateUtil.buildSessionFactory();

        List listaEmpleados = modelo.getEmpleados();
        int numeroEmpleadosInicial = listaEmpleados.size();

        modelo.guardarEmpleado(this.empleadoAProbar);
        listaEmpleados = modelo.getEmpleados();
        int numeroEmpleadosActual = listaEmpleados.size();

        Assert.assertEquals(numeroEmpleadosInicial + 1, numeroEmpleadosActual);
    }

    @Test
    /**
     * Método encargado de probar el funcionamiento de guardar un cliente
     */
    public void guardarClienteTest() {
        HibernateUtil.buildSessionFactory();

        List listaClientes = modelo.getClientes();
        int numeroClientesInicial = listaClientes.size();

        modelo.guardarCliente(this.clienteAProbar);
        listaClientes = modelo.getClientes();
        int numeroClientesActual = listaClientes.size();

        Assert.assertEquals(numeroClientesInicial + 1, numeroClientesActual);
    }

    @Test
    /**
     * Método encargado de probar el funcionamiento de guardar un videojuego
     */
    public void guardarVideojuegoTest() {
        HibernateUtil.buildSessionFactory();

        List listaVideojuegos = modelo.getVideojuegos();
        int numeroVideojuegosInicial = listaVideojuegos.size();

        modelo.guardarVideojuego(this.videojuegoAProbar);
        listaVideojuegos = modelo.getTiendas();
        int numeroVideojuegosActual = listaVideojuegos.size();

        Assert.assertEquals(numeroVideojuegosInicial + 1, numeroVideojuegosActual);
    }

    @Test
    /**
     * Método encargado de probar el funcionamiento de eliminar una tienda
     */
    public void eliminarTiendaTest() {
        HibernateUtil.buildSessionFactory();

        List listaTiendas = modelo.getTiendas();
        int numeroTiendasInicial = listaTiendas.size();

        Tienda tiendaAEliminar = modelo.getTiendasBuscar("TiendaAProbar", "nombre").get(0);

        modelo.eliminarTienda(tiendaAEliminar);
        listaTiendas = modelo.getTiendas();
        int numeroTiendasActual = listaTiendas.size();

        Assert.assertEquals(numeroTiendasInicial - 1, numeroTiendasActual);
    }

    @Test
    /**
     * Método encargado de probar el funcionamiento de eliminar un empleado
     */
    public void eliminarEmpleadoTest() {
        HibernateUtil.buildSessionFactory();

        List listaEmpleados = modelo.getEmpleados();
        int numeroEmpleadosInicial = listaEmpleados.size();

        Empleado empleadoAEliminar = modelo.getEmpleadosBuscar("EmpleadoPrueba", "nombre").get(0);

        modelo.eliminarEmpleado(empleadoAEliminar);
        listaEmpleados = modelo.getEmpleados();
        int numeroEmpleadosActual = listaEmpleados.size();

        Assert.assertEquals(numeroEmpleadosInicial - 1, numeroEmpleadosActual);
    }

    @Test
    /**
     * Método encargado de probar el funcionamiento de eliminar un cliente
     */
    public void eliminarClienteTest() {
        HibernateUtil.buildSessionFactory();

        List listaClientes = modelo.getClientes();
        int numeroClientesInicial = listaClientes.size();

        Cliente clienteAEliminar = modelo.getClientesBuscar("ClientePrueba", "nombre").get(0);

        modelo.eliminarCliente(clienteAEliminar);
        listaClientes = modelo.getClientes();
        int numeroClientesActual = listaClientes.size();

        Assert.assertEquals(numeroClientesInicial - 1, numeroClientesActual);
    }

    @Test
    /**
     * Método encargado de probar el funcionamiento de eliminar un videojuego
     */
    public void eliminarVideojuegoTest() {
        HibernateUtil.buildSessionFactory();

        List listaVideojuegos = modelo.getVideojuegos();
        int numeroVideojuegosInicial = listaVideojuegos.size();

        Videojuego videojuegoAEliminar = modelo.getVideojuegosBuscar("VideojuegoPrueba", "nombre").get(0);

        modelo.guardarVideojuego(videojuegoAEliminar);
        listaVideojuegos = modelo.getTiendas();
        int numeroVideojuegosActual = listaVideojuegos.size();

        Assert.assertEquals(numeroVideojuegosInicial + 1, numeroVideojuegosActual);
    }

}
