package com.vcmanagement.util;

import javax.swing.*;

public class Util {
    /*
        Mostrar mensaje de error con el mensaje indicado
     */
    public static void mensajeError(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje,"Error", JOptionPane.ERROR_MESSAGE);
    }

    /*
        Mostrar mensaje informativo con el mensaje indicado
     */
    public static void mensajeInformativo(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje,"Éxito", JOptionPane.INFORMATION_MESSAGE);
    }

}
